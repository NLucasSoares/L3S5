#ifndef PROJET_SERVEUR_PROTECT
#define PROJET_SERVEUR_PROTECT

/*
	Header du projet serveur

	@author SOARES Lucas
*/

// -------------------------
// namespace Projet::Serveur
// -------------------------

// Temps avant d'abandonner le lancement si kick joueur incomplet
static const NU32 BTEMPS_AVANT_TIMEOUT_SERVEUR_KICK_JOUEUR_NON_PRET_DEBUT_PRET = 2000;

/* Main */
NS32 Projet_Serveur_Main( void );

// namespace Projet::Serveur::Reseau
#include "Reseau/Projet_Serveur_Reseau.h"

// enum Projet::Serveur::BEtatServeur
#include "Projet_Serveur_BEtatServeur.h"

// namespace Projet::Serveur::Monde
#include "Monde/Projet_Serveur_Monde.h"

// struct Projet::Serveur::BServeur
#include "Projet_Serveur_BServeur.h"

// namespace Projet::Serveur::TraitementPacket
#include "TraitementPacket/Projet_Serveur_TraitementPacket.h"

// enum Projet::Serveur::BEtapeConstructionServeur
#include "Projet_Serveur_BEtapeConstructionServeur.h"

#endif // !PROJET_SERVEUR_PROTECT

