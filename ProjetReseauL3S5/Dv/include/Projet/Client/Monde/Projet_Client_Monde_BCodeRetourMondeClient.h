#ifndef PROJET_CLIENT_MONDE_BCODERETOURMONDECLIENT_PROTECT
#define PROJET_CLIENT_MONDE_BCODERETOURMONDECLIENT_PROTECT

// --------------------------------------------------
// enum Projet::Client::Monde::BCodeRetourMondeClient
// --------------------------------------------------

typedef enum BCodeRetourMondeClient
{
	BCODE_RETOUR_MONDE_CLIENT_RETOUR_MENU_PRINCIPAL,

	BCODE_RETOUR_MONDE_CLIENT_QUITTER,

	BCODES_RETOUR_MONDE_CLIENT
} BCodeRetourMondeClient;

#endif // !PROJET_CLIENT_MONDE_BCODERETOURMONDECLIENT_PROTECT
