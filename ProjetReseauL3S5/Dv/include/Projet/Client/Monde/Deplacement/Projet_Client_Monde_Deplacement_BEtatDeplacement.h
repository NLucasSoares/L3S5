#ifndef PROJET_CLIENT_MONDE_DEPLACEMENT_BETATDEPLACEMENT_PROTECT
#define PROJET_CLIENT_MONDE_DEPLACEMENT_BETATDEPLACEMENT_PROTECT

// -----------------------------------------------------------
// struct Projet::Client::Monde::Deplacement::BEtatDeplacement
// -----------------------------------------------------------

typedef struct BEtatDeplacement
{
	// Est d�placement en cours?
	NBOOL m_estEnCours;

	// Est attente r�ponse d�placement?
	NBOOL m_estAttenteReponseDeplacement;

	// D�but pression touche
	NU32 m_tempsDebutPressionTouche;

	// Direction
	NDirection m_direction;

	// Client
	const struct BClient *m_client;
} BEtatDeplacement;

/* Construire */
__ALLOC BEtatDeplacement *Projet_Client_Monde_Deplacement_BEtatDeplacement_Construire( const struct BClient* );

/* D�truire */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Detruire( BEtatDeplacement** );

/* D�placer */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Deplacer( BEtatDeplacement*,
	NDirection direction );

/* Stopper d�placement */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_StopperDeplacement( BEtatDeplacement*,
	NDirection direction );

/* R�ponse d�placement serveur */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_EnregistrerReponseDeplacementServeur( BEtatDeplacement* );

/* Est en attente de la r�ponse pour le d�placement? */
NBOOL Projet_Client_Monde_Deplacement_BEtatDeplacement_EstAttenteReponseServeur( const BEtatDeplacement* );

/* Update */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Update( BEtatDeplacement* );

#endif // !PROJET_CLIENT_MONDE_DEPLACEMENT_BETATDEPLACEMENT_PROTECT

