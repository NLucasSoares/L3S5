#ifndef PROJET_CLIENT_TRAITEMENTPACKET_PROTECT
#define PROJET_CLIENT_TRAITEMENTPACKET_PROTECT

// ------------------------------------------
// namespace Projet::Client::TraitementPacket
// friend Projet::Client::BClient
// ------------------------------------------

/* Envoyer requ�te ping */
NBOOL Projet_Client_TraitementPacket_EnvoyerRequetePing( BClient* );

/* Envoyer les informations du joueur (priv�e) */
NBOOL Projet_Client_TraitementPacket_EnvoyerInformationsJoueur( BClient*,
	const char *nom,
	NU32 charset,
	NU32 couleurCharset );

/* Envoyer changement �tat pr�t */
NBOOL Projet_Client_TraitementPacket_EnvoyerChangementEtatPret( BClient* );

/* Envoyer lancement partie effectu� */
NBOOL Projet_Client_TraitementPacket_EnvoyerLancementEffectue( BClient* );

/* Envoyer changement direction */
NBOOL Projet_Client_TraitementPacket_EnvoyerChangementDirection( BClient*,
	NDirection );

/* Envoyer changement position */
NBOOL Projet_Client_TraitementPacket_EnvoyerChangementPosition( BClient*,
	NDirection );

/* Envoyer pose bombe */
NBOOL Projet_Client_TraitementPacket_EnvoyerPoseBombe( BClient* );

// BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT
NBOOL Projet_Client_TraitementPacket_TraiterConnexionTransmetIdentifiant( BClient*,
	const struct BPacketServeurClientConnexionTransmetIdentifiant* );

// BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR
NBOOL Projet_Client_TraitementPacket_TraiterReponseInformationsJoueur( BClient*,
	const struct BPacketServeurClientReponseInformationsJoueur* );

// BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR
NBOOL Projet_Client_TraitementPacket_TraiterDeconnexionJoueur( BClient*,
	const struct BPacketServeurClientDeconnexionJoueur* );

// BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT
NBOOL Projet_Client_TraitementPacket_TraiterChecksumIncorrect( BClient*,
	const struct BPacketServeurClientChecksumIncorrect* );

// BTYPE_PACKET_SERVEUR_CLIENT_PING
NBOOL Projet_Client_TraitementPacket_TraiterPing( BClient*,
	const struct BPacketServeurClientPing* );
// BTYPE_PACKET_SERVEUR_CLIENT_PONG
NBOOL Projet_Client_TraitementPacket_TraiterPong( BClient*,
	const struct BPacketServeurClientPong* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseEtatPret( BClient*,
	const struct BPacketServeurClientDiffuseEtatPret* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseChangementCarte( BClient*,
	const struct BPacketServeurClientDiffuseChangementCarte* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseLancement( BClient*,
	const struct BPacketServeurClientDiffuseLancement* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseMessageAfficher( BClient*,
	const struct BPacketServeurClientDiffuseMessageAfficher* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseDebutPartie( BClient*,
	const struct BPacketServeurClientDiffuseDebutPartie* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseChangementDirection( BClient*,
	const struct BPacketServeurClientDiffuseChangementDirection* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseChangementPosition( BClient*,
	const struct BPacketServeurClientDiffuseChangementPosition* );

// BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE
NBOOL Projet_Client_TraitementPacket_TraiterRefusePoseBombe( BClient*,
	const struct BPacketServeurClientRefusePoseBombe* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE
NBOOL Projet_Client_TraitementPacket_TraiterDiffusePoseBombe( BClient*,
	const struct BPacketServeurClientDiffusePoseBombe* );
// BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE
NBOOL Projet_Client_TraitementPacket_TraiterBombeExplose( BClient*,
	const struct BPacketServeurClientBombeExplose* );
// BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT
NBOOL Projet_Client_TraitementPacket_TraiterBlocRempliDetruit( BClient*,
	const struct BPacketServeurClientBlocRempliDetruit* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseApparitionBonus( BClient*,
	const struct BPacketServeurClientDiffuseApparitionBonus* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseDisparitionBonus( BClient*,
	const struct BPacketServeurClientDiffuseDisparitionBonus* );
// BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS
NBOOL Projet_Client_TraitementPacket_TraiterDistribueBonus( BClient*,
	const struct BPacketServeurClientDistribueBonus* );

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseMortJoueur( BClient*,
	const struct BPacketServeurClientDiffuseMortJoueur* );

// BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE
NBOOL Projet_Client_TraitementPacket_TraiterAnnonceFinPartie( BClient*,
	const struct BPacketServeurClientAnnonceFinPartie* );

#endif // !PROJET_CLIENT_TRAITEMENTPACKET_PROTECT

