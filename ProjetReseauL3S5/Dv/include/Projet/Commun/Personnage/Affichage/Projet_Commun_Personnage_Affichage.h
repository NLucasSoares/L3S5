#ifndef PROJET_COMMUN_PERSONNAGE_AFFICHAGE_PROTECT
#define PROJET_COMMUN_PERSONNAGE_AFFICHAGE_PROTECT

// -----------------------------------------------
// namespace Projet::Commun::Personnage::Affichage
// -----------------------------------------------

/* Afficher personnages */
NBOOL Projet_Commun_Personnage_Affichage_Afficher( BCacheClient*,
	NBOOL estAttenteReponseDeplacementClientCourant,
	BPersonnage**,
	NSPoint baseAffichage,
	NU32 zoom );

#endif // !PROJET_COMMUN_PERSONNAGE_AFFICHAGE_PROTECT

