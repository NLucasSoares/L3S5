#ifndef PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERCHARSET_PROTECT
#define PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERCHARSET_PROTECT

// -------------------------------------------------------------
// enum Projet::Commun::Fichier::Personnage::BClefFichierCharset
// -------------------------------------------------------------

typedef enum BClefFichierCharset
{
	BCLEF_FICHIER_CHARSET_NOMBRE_COULEURS,
	BCLEF_FICHIER_CHARSET_COULEURS,

	BCLEFS_FICHIER_CHARSET
} BClefFichierCharset;

/* Obtenir clef */
const char *Projet_Commun_Fichier_Personnage_BClefFichierCharset_ObtenirClef( BClefFichierCharset );

/* Obtenir ensemble des clefs */
__ALLOC char **Projet_Commun_Fichier_Personnage_BClefFichierCharset_ObtenirEnsembleClef( void );

#ifdef PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERCHARSET_INTERNE
static const char BCLEF_FICHIER_CHARSET_TEXTE[ BCLEFS_FICHIER_CHARSET ][ 32 ] =
{
	"Nombre couleurs: ",
	"Couleurs: "
};
#endif // PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERCHARSET_INTERNE

#endif // !PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERCHARSET_PROTECT

