#ifndef PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERPERSONNAGE_PROTECT
#define PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERPERSONNAGE_PROTECT

/*
	Les clefs pr�sentes dans la d�finition d'un personnage

	@author SOARES Lucas
*/

// ----------------------------------------------------------------
// enum Projet::Commun::Fichier::Personnage::BClefFichierPersonnage
// ----------------------------------------------------------------

typedef enum BClefFichierPersonnage
{
	BCLEF_FICHIER_PERSONNAGE_NOM,
	BCLEF_FICHIER_PERSONNAGE_CHARSET,
	BCLEF_FICHIER_PERSONNAGE_VITESSE,
	BCLEF_FICHIER_PERSONNAGE_TAUX_DROP,

	BCLEFS_FICHIER_PERSONNAGE
} BClefFichierPersonnage;

/* Obtenir clef */
const char *Projet_Commun_Fichier_Personnage_BClefFichierPersonnage_ObtenirClef( BClefFichierPersonnage );

/* Compose un tableau de l'ensemble des clefs */
__ALLOC char **Projet_Commun_Fichier_Personnage_BClefFichierPersonnage_ObtenirEnsembleClef( void );

#ifdef PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERPERSONNAGE_INTERNE
static const char BCLEF_FICHIER_PERSONNAGE_TEXTE[ BCLEFS_FICHIER_PERSONNAGE ][ 32 ] =
{
	"Nom: ",
	"Charset: ",
	"Vitesse: ",
	"Taux drop: "
};
#endif // PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERPERSONNAGE_INTERNE

#endif // !PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERPERSONNAGE_PROTECT

