#ifndef PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOSITIONAFFICHAGEMESSAGECLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOSITIONAFFICHAGEMESSAGECLIENT_PROTECT

// -----------------------------------------------------------------------------
// enum Projet::Commun::Reseau::Client::Message::BPositionAffichageMessageClient
// -----------------------------------------------------------------------------

typedef enum BPositionAffichageMessageClient
{
	BPOSITION_AFFICHAGE_MESSAGE_CLIENT_CENTRE,
	BPOSITION_AFFICHAGE_MESSAGE_CLIENT_HAUT,

	BPOSITIONS_AFFICHAGE_MESSAGE_CLIENT,

	BPOSITION_AFFICHAGE_MESSAGE_CLIENT_DEFAUT = BPOSITION_AFFICHAGE_MESSAGE_CLIENT_CENTRE
} BPositionAffichageMessageClient;

// V�rifier taille
nassert( sizeof( BPositionAffichageMessageClient ) == sizeof( NU32 ),
	"Projet::Commun::Reseau::Client::Message::BPositionAffichageMessageClient" );

#endif // !PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOSITIONAFFICHAGEMESSAGECLIENT_PROTECT

