#ifndef PROJET_COMMUN_RESEAU_CLIENT_BETATDEPLACEMENTCLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_BETATDEPLACEMENTCLIENT_PROTECT

// -------------------------------------------------------------
// struct Projet::Commun::Reseau::Client::BEtatDeplacementClient
// -------------------------------------------------------------

typedef struct BEtatDeplacementClient
{
	// Position au d�but du d�placement
	NSPoint m_positionInitiale;

	// Scrolling
	NSPoint m_etatScrolling;

	// Dernier changement scrolling
	NU32 m_tempsDernierChangementScrolling;

	// Est d�placement actif?
	NBOOL m_estDeplacementActif;

	// Direction d�placement
	NDirection m_direction;
} BEtatDeplacementClient;

/* Construire */
__ALLOC BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire( void );
__ALLOC BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire2( const BEtatDeplacementClient* );

/* D�truire */
void Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( BEtatDeplacementClient** );

/* D�marrer d�placement */
void Projet_Commun_Reseau_Client_BEtatDeplacementClient_Deplacer( BEtatDeplacementClient*,
	NSPoint positionInitiale,
	NDirection );

/* Obtenir �tat scrolling */
const NSPoint *Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirEtatScrolling( const BEtatDeplacementClient* );

/* Est d�placement actif? */
NBOOL Projet_Commun_Reseau_Client_BEtatDeplacementClient_EstDeplacementActif( const BEtatDeplacementClient* );

/* Obtenir position initiale */
NSPoint Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirPositionInitiale( const BEtatDeplacementClient* );

/* Obtenir direction d�placement */
NDirection Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirDirectionDeplacement( const BEtatDeplacementClient* );

/* Update */
void Projet_Commun_Reseau_Client_BEtatDeplacementClient_Update( BEtatDeplacementClient* );

#endif // !PROJET_COMMUN_RESEAU_CLIENT_BETATDEPLACEMENTCLIENT_PROTECT

