#ifndef PROJET_COMMUN_RESEAU_CLIENT_CACHE_BCACHECLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_CACHE_BCACHECLIENT_PROTECT

// ----------------------------------------------------------
// struct Projet::Commun::Reseau::Client::Cache::BCacheClient
// ----------------------------------------------------------

typedef struct BCacheClient
{
	// Mutex
	NMutex *m_mutex;

	// Identifiant client courant
	NU32 m_identifiantClientCourant;

	// Est cache ferm�?
	NBOOL m_estCacheFerme;

	// Identifiant modification cache
	NU32 m_identifiantModificationCache;

	// Nombre de clients [Interne]
	NU32 m_nombreClient;

	// Etats clients
	BEtatClient *m_etat[ BNOMBRE_MAXIMUM_JOUEUR ];

	// Ressource
	const struct BRessource *m_ressource;
} BCacheClient;

/* Construire */
__ALLOC BCacheClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_Construire( const struct BRessource *ressource );

/* D�truire */
void Projet_Commun_Reseau_Client_Cache_BCacheClient_Detruire( BCacheClient** );

/* Ajouter un client (protection n�cessaire) */
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientCourant( BCacheClient*,
	NU32 identifiant,
	void *donneeSupplementaire );
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientExterne( BCacheClient*,
	NU32 identifiant,
	void *donneeSupplementaire );

/* Fermer le cache (emp�cher connexion) */
void Projet_Commun_Reseau_Client_Cache_BCacheClient_Fermer( BCacheClient* );

/* Ouvrir le cache (autoriser connexion) */
void Projet_Commun_Reseau_Client_Cache_BCacheClient_Ouvrir( BCacheClient* );

/* Supprimer un client (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClientNoLock( BCacheClient*,
	NU32 identifiant );

/* Supprimer un client (protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClient( BCacheClient*,
	NU32 identifiant );

/* Update cache (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_Update( BCacheClient* );

/* Update cache (protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_UpdateNoLock( BCacheClient* );

/* Enregistrer demande personnalisation client (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerDemandePersonnalisation( BCacheClient*,
	NU32 identifiant,
	NU32 charset,
	NU32 couleurCharset,
	const char *nom,
	const struct BRessource *ressource );
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerDemandePersonnalisation2( BCacheClient*,
	NU32 identifiant,
	NU32 charset,
	NU32 couleurCharset,
	const char *nom );

/* Enregistrer changement �tat pr�t (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementEtatPret( BCacheClient*,
	NU32 identifiant );
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementEtatPret2( BCacheClient*,
	NU32 identifiant,
	BEtatPret );

/* Enregistrer confirmation lancement client (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerConfirmationLancement( BCacheClient*,
	NU32 identifiant );

/* Enregistrer changement direction (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementDirection( BCacheClient*,
	NU32 identifiant,
	NDirection );

/* Enregistrer changement position (protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementPosition( BCacheClient*,
	NU32 identifiant,
	NSPoint position,
	NDirection );

/* D�finir si le client est v�rifi� (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_DefinirClientVerifie( BCacheClient*,
	NU32 identifiant );

/* Obtenir identifiant client courant (aucune protection n�cessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( const BCacheClient* );

/* Obtenir un client (protection n�cessaire) */
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( const BCacheClient*,
	NU32 identifiant );
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( const BCacheClient*,
	NU32 index );
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClientCourant( const BCacheClient* );

/* Obtenir un index client (protection n�cessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClientCourant( const BCacheClient* );
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( const BCacheClient*,
	NU32 identifiant );

/* Obtenir nombre clients (protection n�cessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( const BCacheClient* );

/* Obtenir identifiant derni�re modification (aucune protection n�cessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantDerniereModification( const BCacheClient* );

/* Est clients pr�ts? (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EstClientsPrets( const BCacheClient* );

/* Vider (protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_ViderNoLock( BCacheClient* );

/* Vider (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_Vider( BCacheClient* );

/* Vider (protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_ViderNoLock( BCacheClient* );

/* Envoyer un packet (aucune protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( BCacheClient*,
	__WILLBEOWNED NPacket* );

/* Envoyer un packet (protection n�cessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTous( BCacheClient*,
	__WILLBEOWNED NPacket* );

/* Obtenir nombre de clients morts (protection n�cessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClientMort( const BCacheClient* );

/* Prot�ger cache (protection activ�e) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( BCacheClient* );

/* Ne plus prot�ger cache (protection d�sactiv�e) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( BCacheClient* );

#endif // !PROJET_COMMUN_RESEAU_CLIENT_CACHE_BCACHECLIENT_PROTECT

