#ifndef PROJET_COMMUN_CARTE_BLOC_BTYPEBLOC_PROTECT
#define PROJET_COMMUN_CARTE_BLOC_BTYPEBLOC_PROTECT

/*
	Liste des diff�rents types de blocs sur une carte

	@author SOARES Lucas
*/

// -------------------------------------------
// enum Projet::Commun::Carte::Bloc::BTypeBloc
// -------------------------------------------

typedef enum BTypeBloc
{
	BTYPE_BLOC_VIDE,
	BTYPE_BLOC_SOLIDE,
	BTYPE_BLOC_REMPLISSABLE,

	BTYPES_BLOC
} BTypeBloc;

// V�rifier la taille
nassert( sizeof( BTypeBloc ) == sizeof( NU32 ),
	"Projet_Commun_Carte_Bloc_BTypeBloc" );

#endif // !PROJET_COMMUN_CARTE_BLOC_BTYPEBLOC_PROTECT

