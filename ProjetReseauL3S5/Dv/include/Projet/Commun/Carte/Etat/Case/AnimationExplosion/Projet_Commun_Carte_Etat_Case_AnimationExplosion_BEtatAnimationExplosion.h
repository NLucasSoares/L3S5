#ifndef PROJET_COMMUN_CARTE_ETAT_CASE_ANIMATIONEXPLOSION_BETATANIMATIONEXPLOSION_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_CASE_ANIMATIONEXPLOSION_BETATANIMATIONEXPLOSION_PROTECT

// -------------------------------------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::AnimationExplosion::BEtatAnimationExplosion
// -------------------------------------------------------------------------------------

typedef struct BEtatAnimationExplosion
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Etat de l'animation
	NEtatAnimation *m_etat;

	// Type de train�e (BContenuAnimationBombe)
	NU32 m_typeAnimation;

	// Identifiant joueur
	NU32 m_identifiantJoueur;

	// Animation explosion
	const NAnimation *m_animation;
} BEtatAnimationExplosion;

/* Construire */
__ALLOC BEtatAnimationExplosion *Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Construire( const NAnimation *animationBombe );

/* D�truire */
void Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Detruire( BEtatAnimationExplosion** );

/* Update */
void Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Update( BEtatAnimationExplosion* );

/* Activer l'animation */
void Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( BEtatAnimationExplosion*,
	NU32 typeAnimation,
	NU32 identifiantJoueur );

/* Obtenir animation */
const NAnimation *Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirAnimation( const BEtatAnimationExplosion* );

/* Obtenir type animation */
NU32 Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirTypeAnimation( const BEtatAnimationExplosion* );

/* Obtenir frame animation */
NU32 Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirFrameAnimation( const BEtatAnimationExplosion* );

/* Obtenir identifiant joueur */
NU32 Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirIdentifiantJoueur( const BEtatAnimationExplosion* );

/* Est en cours? */
NBOOL Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_EstEnCours( const BEtatAnimationExplosion* );

#endif // !PROJET_COMMUN_CARTE_ETAT_CASE_ANIMATIONEXPLOSION_BETATANIMATIONEXPLOSION_PROTECT

