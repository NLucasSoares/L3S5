#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BLISTEEFFETSONORE_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BLISTEEFFETSONORE_PROTECT

// ---------------------------------------------------------------
// enum Projet::Commun::Ressource::Audio::Effet::BListeEffetSonore
// ---------------------------------------------------------------

typedef enum BListeEffetSonore
{
	BLISTE_EFFET_SONORE_BM_BOMB1,
	BLISTE_EFFET_SONORE_BM_BOMB2,
	BLISTE_EFFET_SONORE_BM_BOMB3,
	BLISTE_EFFET_SONORE_BM_BOMB4,
	BLISTE_EFFET_SONORE_BM_JEU_MORT,
	BLISTE_EFFET_SONORE_BM_JEU_PICKUP,
	BLISTE_EFFET_SONORE_BM_JEU_TIMEUP,
	BLISTE_EFFET_SONORE_BM_MENU_CONFIRM,
	BLISTE_EFFET_SONORE_BM_MENU_PAUSE,
	BLISTE_EFFET_SONORE_BM_MENU_RESET,
	BLISTE_EFFET_SONORE_BM_MENU_SELECT,
	BLISTE_EFFET_SONORE_BM_POWERUP1,
	BLISTE_EFFET_SONORE_BM_POWERUP2,
	BLISTE_EFFET_SONORE_BM_POWERUP3,
	BLISTE_EFFET_SONORE_BM_POWERUP4,
	BLISTE_EFFET_SONORE_BM_POWERUP5,

	BLISTE_EFFET_SONORE_BM_POWERDOWN1,

	BLISTE_EFFETS_SONORE
} BListeEffetSonore;

/* Obtenir lien effet sonore */
const char *Projet_Commun_Ressource_Audio_Effet_BListeEffetSonore_ObtenirLien( BListeEffetSonore );

#ifdef PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BLISTEEFFETSONORE_INTERNE
static const char BListeEffetSonoreTexte[ BLISTE_EFFETS_SONORE ][ 64 ] =
{
	"Assets/Audio/SE/BM_Bomb1.mp3",
	"Assets/Audio/SE/BM_Bomb2.mp3",
	"Assets/Audio/SE/BM_Bomb3.mp3",
	"Assets/Audio/SE/BM_Bomb4.mp3",
	"Assets/Audio/SE/BM_Jeu_Mort.mp3",
	"Assets/Audio/SE/BM_Jeu_PickUp.mp3",
	"Assets/Audio/SE/BM_Jeu_TimeUp.mp3",
	"Assets/Audio/SE/BM_Menu_Confirm.mp3",
	"Assets/Audio/SE/BM_Menu_Pause.mp3",
	"Assets/Audio/SE/BM_Menu_Reset.mp3",
	"Assets/Audio/SE/BM_Menu_Select.mp3",
	"Assets/Audio/SE/BM_PowerUp1.mp3",
	"Assets/Audio/SE/BM_PowerUp2.mp3",
	"Assets/Audio/SE/BM_PowerUp3.mp3",
	"Assets/Audio/SE/BM_PowerUp4.mp3",
	"Assets/Audio/SE/BM_PowerUp5.mp3",
	"Assets/Audio/SE/BM_PowerDown1.mp3"
};
#endif // PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BLISTEEFFETSONORE_INTERNE

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BLISTEEFFETSONORE_PROTECT

