#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BMUSIQUE_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BMUSIQUE_PROTECT

// ----------------------------------------------------------
// struct Projet::Commun::Ressource::Audio::Musique::BMusique
// ----------------------------------------------------------

typedef struct BMusique
{
	// Musiques
	NMusique *m_musique[ BLISTE_MUSIQUES ];

	// Musique en cours de lecture
	BListeMusique m_musiqueEnCours;
} BMusique;

/* Construire */
__ALLOC BMusique *Projet_Commun_Ressource_Audio_Musique_BMusique_Construire( void );

/* D�truire */
void Projet_Commun_Ressource_Audio_Musique_BMusique_Detruire( BMusique** );

/* Lire musique */
NBOOL Projet_Commun_Ressource_Audio_Musique_BMusique_Lire( BMusique*,
	BListeMusique );

/* Est lecture en cours? */
NBOOL Projet_Commun_Ressource_Audio_Musique_BMusique_EstLecture( const BMusique* );

/* Arr�ter musique */
NBOOL Projet_Commun_Ressource_Audio_Musique_BMusique_Arreter( BMusique* );

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BMUSIQUE_PROTECT

