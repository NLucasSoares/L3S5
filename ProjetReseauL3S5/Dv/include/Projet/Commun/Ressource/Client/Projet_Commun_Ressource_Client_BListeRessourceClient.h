#ifndef PROJET_COMMUN_RESSOURCE_CLIENT_BLISTERESSOURCECLIENT_PROTECT
#define PROJET_COMMUN_RESSOURCE_CLIENT_BLISTERESSOURCECLIENT_PROTECT

// -------------------------------------------------------------
// enum Projet::Commun::Ressource::Client::BListeRessourceClient
// -------------------------------------------------------------

typedef enum BListeRessourceClient
{
	// Menus
	BLISTE_RESSOURCE_CLIENT_MENU_PRINCIPAL_BOMBERMAN,

	BLISTE_RESSOURCES_CLIENT
} BListeRessourceClient;

/* Obtenir le lien */
const char *Projet_Commun_Ressource_Client_BListeRessourceClient_ObtenirLien( BListeRessourceClient );

#ifdef PROJET_COMMUN_RESSOURCE_CLIENT_BLISTERESSOURCECLIENT_INTERNE
static const char BListeRessourceClientTexte[ BLISTE_RESSOURCES_CLIENT ][ 64 ] =
{
	"Assets/Menu/Principal/Bomberman.png"
};
#endif // PROJET_COMMUN_RESSOURCE_CLIENT_BLISTERESSOURCECLIENT_INTERNE

#endif // !PROJET_COMMUN_RESSOURCE_CLIENT_BLISTERESSOURCECLIENT_PROTECT