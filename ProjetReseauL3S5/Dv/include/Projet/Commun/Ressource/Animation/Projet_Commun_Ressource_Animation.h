#ifndef PROJET_COMMUN_RESSOURCE_ANIMATION_PROTECT
#define PROJET_COMMUN_RESSOURCE_ANIMATION_PROTECT

// ----------------------------------------------
// namespace Projet::Commun::Ressource::Animation
// ----------------------------------------------

// enum Projet::Commun::Ressource::Animation::BListeAnimation
#include "Projet_Commun_Ressource_Animation_BListeAnimation.h"

// namespace Projet::Commun::Ressource::Animation::Contenu
#include "Contenu/Projet_Commun_Ressource_Animation_Contenu.h"

// struct Projet::Commun::Ressource::Animation::BEnsembleAnimation
#include "Projet_Commun_Ressource_Animation_BEnsembleAnimation.h"

#endif // !PROJET_COMMUN_RESSOURCE_ANIMATION_PROTECT

