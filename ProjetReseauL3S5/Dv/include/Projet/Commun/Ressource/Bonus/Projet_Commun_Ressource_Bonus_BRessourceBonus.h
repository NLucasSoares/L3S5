#ifndef PROJET_COMMUN_RESSOURCE_BONUS_BRESSOURCEBONUS_PROTECT
#define PROJET_COMMUN_RESSOURCE_BONUS_BRESSOURCEBONUS_PROTECT

// --------------------------------------------------------
// struct Projet::Commun::Ressource::Bonus::BRessourceBonus
// --------------------------------------------------------

typedef struct BRessourceBonus
{
	// Ic�ne
	NTileset *m_bonus;
} BRessourceBonus;

/* Construire */
__ALLOC BRessourceBonus *Projet_Commun_Ressource_Bonus_BRessourceBonus_Construire( const char*,
	const NFenetre *fenetre );

/* D�truire */
void Projet_Commun_Ressource_Bonus_BRessourceBonus_Detruire( BRessourceBonus** );

/* Obtenir ic�ne */
const NSurface *Projet_Commun_Ressource_Bonus_BRessourceBonus_Obtenir( const BRessourceBonus*,
	BListeBonus );

#endif // !PROJET_COMMUN_RESSOURCE_BONUS_BRESSOURCEBONUS_PROTECT

