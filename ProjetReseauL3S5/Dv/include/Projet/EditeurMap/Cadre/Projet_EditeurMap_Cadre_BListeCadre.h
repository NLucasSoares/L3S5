#ifndef PROJET_EDITEURMAP_CADRE_BLISTECADRE_PROTECT
#define PROJET_EDITEURMAP_CADRE_BLISTECADRE_PROTECT

// -------------------------------------------
// enum Projet::EditeurMap::Cadre::BListeCadre
// -------------------------------------------

typedef enum BListeCadre
{
	BLISTE_CADRE_CARTE,
	BLISTE_CADRE_TILESET,
	BLISTE_CADRE_TYPE_BLOC,

	BLISTE_CADRES
} BListeCadre;

#endif // !PROJET_EDITEURMAP_CADRE_BLISTECADRE_PROTECT

