#ifndef PROJET_EDITEURMAP_CADRE_TYPEBLOC_PROTECT
#define PROJET_EDITEURMAP_CADRE_TYPEBLOC_PROTECT

// ---------------------------------------------
// namespace Projet::EditeurMap::Cadre::TypeBloc
// ---------------------------------------------

static const NCouleur BCOULEUR_TYPE_BLOC_EDITEUR[ BTYPES_BLOC ] =
{
	{ 0x00, 0xFF, 0x00, 0x7F },
	{ 0xFF, 0x00, 0x00, 0x7F },
	{ 0x7F, 0x7F, 0x00, 0x7F }
};

#endif // !PROJET_EDITEURMAP_CADRE_TYPEBLOC_PROTECT

