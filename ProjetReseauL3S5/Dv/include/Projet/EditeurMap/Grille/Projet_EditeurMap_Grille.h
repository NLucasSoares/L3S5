#ifndef PROJET_EDITEURMAP_GRILLE_PROTECT
#define PROJET_EDITEURMAP_GRILLE_PROTECT

// ------------------------------------
// namespace Projet::EditeurMap::Grille
// ------------------------------------

// Dimension horizontale de la grille
static const NU32 BDIMENSION_HORIZONTALE_GRILLE_EDITEUR = 15;

// enum Projet::EditeurMap::Grille::BTypeGrille
#include "Projet_EditeurMap_Grille_BTypeGrille.h"

// struct Projet::EditeurMap::Grille::BCaseGrille
#include "Projet_EditeurMap_Grille_BCaseGrille.h"

// struct Projet::EditeurMap::Grille::BGrille
#include "Projet_EditeurMap_Grille_BGrille.h"

#endif // !PROJET_EDITEURMAP_GRILLE_PROTECT

