#ifndef PROJET_EDITEURMAP_GRILLE_BGRILLE_PROTECT
#define PROJET_EDITEURMAP_GRILLE_BGRILLE_PROTECT

// ------------------------------------------
// struct Projet::EditeurMap::Grille::BGrille
// ------------------------------------------

typedef struct BGrille
{
	// Type de la grille
	BTypeGrille m_type;

	// Cases [ 15 ][ x ]
	BCaseGrille **m_cases;

	// Taille
	NUPoint m_taille;
} BGrille;

/* Construire */
__ALLOC BGrille *Projet_EditeurMap_Grille_BGrille_Construire( const BTileset **tileset,
	NU32 nombreTileset );
__ALLOC BGrille *Projet_EditeurMap_Grille_BGrille_Construire2( NU32 nombreTypesBloc );

/* D�truire */
void Projet_EditeurMap_Grille_BGrille_Detruire( BGrille** );

/* Obtenir la taille */
NUPoint Projet_EditeurMap_Grille_BGrille_ObtenirTaille( const BGrille* );

/* Obtenir les cases */
const BCaseGrille **Projet_EditeurMap_Grille_BGrille_ObtenirCase( const BGrille* );

/* Chercher la position d'une case */
NUPoint Projet_EditeurMap_Grille_ChercherPositionCaseTileset( const BGrille *this,
	NU32 tileset,
	NUPoint positionTileset );
NUPoint Projet_EditeurMap_Grille_ChercherPositionCaseTypeBloc( const BGrille *this,
	BTypeBloc );

#endif // !PROJET_EDITEURMAP_GRILLE_BGRILLE_PROTECT

