#ifndef PROJET_EDITEURMAP_BEDITEUR_PROTECT
#define PROJET_EDITEURMAP_BEDITEUR_PROTECT

// -----------------------------------
// struct Projet::EditeurMap::BEditeur
// -----------------------------------

typedef struct BEditeur
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Est �dition de map en cours?
	NBOOL m_estEditionEnCours;

	// Handle du thread de commandes
	HANDLE m_threadCommande;

	// Carte en cours d'�dition
	BCarte *m_carte;

	// Cadres
	NBouton *m_cadre[ BLISTE_CADRES ];

	// Ressources
	BRessource *m_ressource;

	// Grilles
	BGrille *m_grilleTileset;
	BGrille *m_grilleTypeBloc;

	// Position de la souris
	NSPoint m_positionSouris;

	// Scroll tileset
	NUPoint m_scroll[ BLISTE_CADRES ];

	// Selection bloc
	NUPoint m_selectionBloc[ BCOUCHES_CARTE ];
	NCadre *m_cadreSelectionBloc[ BCOUCHES_CARTE ];
	NUPoint m_selectionTypeBloc;

	// Cadre entourant la carte
	NCadre *m_cadreCarte;

	// Cadre type de bloc
	NCadre *m_cadreBlocCarte;

	// Type d'�dition
	BTypeEdition m_typeEdition;

	// Position en cours de choix
	NU32 m_choixPositionDepartJoueurChoix;
	NSurface *m_surfacePositionJoueur[ BNOMBRE_MAXIMUM_JOUEUR ];

	// Surfaces outils
	NSurface *m_surfaceOutil[ BTYPES_EDITION ];

	// Fen�tre
	NFenetre *m_fenetre;
} BEditeur;

/* Construire l'�diteur */
__ALLOC BEditeur *Projet_EditeurMap_BEditeur_Construire( void );

/* D�truire l'�diteur */
void Projet_EditeurMap_BEditeur_Detruire( BEditeur** );

/* Editer */
NBOOL Projet_EditeurMap_BEditeur_Editer( BEditeur* );

/* Cr�er map */
void Projet_EditeurMap_BEditeur_CreerCarte( BEditeur*,
	NU32 handle,
	const char *nom,
	NUPoint taille );

/* Charger */
NBOOL Projet_EditeurMap_BEditeur_Charger( BEditeur*,
	NU32 handle );

/* Sauvegarder */
NBOOL Projet_EditeurMap_BEditeur_Sauvegarder( const BEditeur* );

/* Arr�ter l'�dition */
void Projet_EditeurMap_BEditeur_Arreter( BEditeur* );

/* Changer nom carte */
void Projet_EditeurMap_BEditeur_ChangerNomCarte( BEditeur*,
	const char* );

/* Changer taille carte */
void Projet_EditeurMap_BEditeur_ChangerTailleCarte( BEditeur*,
	NUPoint taille );

/* Afficher informations sur la carte */
void Projet_EditeurMap_BEditeur_AfficherInfosCarte( const BEditeur* );

/* Est en cours? */
NBOOL Projet_EditeurMap_BEditeur_EstEnCours( const BEditeur* );

#endif // !PROJET_EDITEURMAP_BEDITEUR_PROTECT

