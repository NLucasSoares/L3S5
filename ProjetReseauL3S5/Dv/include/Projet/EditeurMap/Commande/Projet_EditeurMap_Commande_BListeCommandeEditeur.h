#ifndef PROJET_EDITEURMAP_COMMANDE_BLISTECOMMANDEEDITEUR_PROTECT
#define PROJET_EDITEURMAP_COMMANDE_BLISTECOMMANDEEDITEUR_PROTECT

// --------------------------------------------------------
// enum Projet::EditeurMap::Commande::BListeCommandeEditeur
// --------------------------------------------------------

typedef enum BListeCommandeEditeur
{
	BLISTE_COMMANDE_EDITEUR_AUCUNE,

	BLISTE_COMMANDE_EDITEUR_QUITTER,

	BLISTE_COMMANDE_EDITEUR_NOUVELLE,
	BLISTE_COMMANDE_EDITEUR_CHARGER,
	BLISTE_COMMANDE_EDITEUR_SAUVEGARDER,

	BLISTE_COMMANDE_EDITEUR_CHANGER_NOM,
	BLISTE_COMMANDE_EDITEUR_CHANGER_TAILLE,

	BLISTE_COMMANDE_EDITEUR_INFOS_CARTE,

	BLISTE_COMMANDE_EDITEUR_AIDE,

	BLISTE_COMMANDES_EDITEUR
} BListeCommandeEditeur;

/* Obtenir commande */
BListeCommandeEditeur Projet_EditeurMap_Commande_BListeCommandeEditeur_Obtenir( const char* );

/* Afficher aide */
void Projet_EditeurMap_Commande_BListeCommandeEditeur_AfficherAide( void );

#ifdef PROJET_EDITEURMAP_COMMANDE_BLISTECOMMANDEEDITEUR_INTERNE
static const char BLISTE_COMMANDE_EDITEUR_TEXTE[ BLISTE_COMMANDES_EDITEUR ][ 32 ] =
{
	"",

	"Quit",

	"New",
	"Load",
	"Save",

	"Rename",
	"Resize",

	"Info",

	"Help"
};

static const char BLISTE_DESCRIPTION_COMMANDE_EDITEUR_TEXTE[ BLISTE_COMMANDES_EDITEUR ][ 64 ] =
{
	"",

	"Ferme le programme",

	"Cree une carte avec des proprietes choisies",
	"Charge une carte deja existante",
	"Sauvegarde la carte en cours",

	"Changer le nom d'une carte",
	"Changer la taille d'une carte",

	"Donne les informations sur la carte en cours",

	"Affiche cette aide"
};
#endif // PROJET_EDITEURMAP_COMMANDE_BLISTECOMMANDEEDITEUR_INTERNE

#endif // !PROJET_EDITEURMAP_COMMANDE_BLISTECOMMANDEEDITEUR_PROTECT

