#ifndef PROJET_EDITEURMAP_PROTECT
#define PROJET_EDITEURMAP_PROTECT

/*
	Header du projet Editeur de Map

	@author SOARES Lucas
*/

// ----------------------------
// namespace Projet::EditeurMap
// ----------------------------

// R�pertoire carte
static const char BREPERTOIRE_CARTE_EDITEUR[ 32 ]						= "Sortie";

// Taille police choix position de d�part
static const NU32 BTAILLE_POLICE_CHOIX_POSITION_DEPART_JOUEUR_EDITEUR	= 16;

// namespace Projet::EditeurMap::Grille
#include "Grille/Projet_EditeurMap_Grille.h"

// namespace Projet::EditeurMap::Cadre
#include "Cadre/Projet_EditeurMap_Cadre.h"

// enum Projet::EditeurMap::BTypeEdition
#include "Projet_EditeurMap_BTypeEdition.h"

// struct Projet::EditeurMap::BEditeur
#include "Projet_EditeurMap_BEditeur.h"

// namespace Projet::EditeurMap::Commande
#include "Commande/Projet_EditeurMap_Commande.h"

// namespace Projet::EditeurMap::Ressource
#include "Ressource/Projet_EditeurMap_Ressource.h"

/* Main �diteur map */
NS32 Projet_EditeurMap_Main( void );

#endif // !PROJET_EDITEURMAP_PROTECT

