#include "../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// DEBUT TEST RESEAU
#ifdef NLIB_MODULE_RESEAU
#define PORT_TEST_RESEAU							16500
#define IP_TEST_RESEAU								"127.0.0.1"
#define NB_PACKETS_CLIENT_SERVEUR_TEST_RESEAU		10
#define NB_PACKETS_SERVEUR_CLIENT_TEST_RESEAU		10
#define NOMBRE_CLIENTS_TEST_RESEAU					30 // MAX = NMAXIMUM_CLIENTS_SIMULTANES_NSERVEUR (30)

NClientServeur *CLIENTS_TEST_RESEAU[ NOMBRE_CLIENTS_TEST_RESEAU ] = { NULL, };

NBOOL receptionPacketClient( const NPacket *packet,
	void *data )
{
	NREFERENCER( data );
	NREFERENCER( packet );
	return NTRUE;
}

NBOOL deconnexionClient( void *data )
{
	NREFERENCER( data );

	printf( "[CLIENT] Le client se deconnecte\n" );
	return NTRUE;
}

NBOOL connexionServeur( const NClientServeur *client )
{
	NU32 i = 0;

	printf( "[SERVEUR] %d connecte\n", client->m_identifiantUnique );

	for( ; i < NOMBRE_CLIENTS_TEST_RESEAU; i++ )
		if( CLIENTS_TEST_RESEAU[ i ] == NULL )
			CLIENTS_TEST_RESEAU[ i ] = (NClientServeur*)client;

	return NTRUE;
}

NBOOL deconnexionServeur( const NClientServeur *client )
{
	printf( "[SERVEUR] %d deconnecte\n", client->m_identifiantUnique );

	return NTRUE;
}

NBOOL receptionPacketServeur( const NClientServeur *client,
	const NPacket *packet )
{
	NREFERENCER( client );
	NREFERENCER( packet );

	return NTRUE;
}

NBOOL test_reseau( void )
{
	NServeur *serveur;

	NClient *client[ NOMBRE_CLIENTS_TEST_RESEAU ];

	char buffer[ 200000 ];
	NU32 i, j;

	printf( "---------------- DEBUT TEST RESEAU ----------------\n" );
	printf( "Taille buffer = %d (avec minimum de %d)\n", NBUFFER_PACKET,
		NBUFFER_PACKET_MINIMUM );
	printf( "Parametres du test Port(%d), Ip(%s), NbPacketClient->Serveur(%d), NbPacketServeur->Client(%d), NbClients(%d [max=%d])\n",
		PORT_TEST_RESEAU,
		IP_TEST_RESEAU,
		NB_PACKETS_CLIENT_SERVEUR_TEST_RESEAU,
		NB_PACKETS_SERVEUR_CLIENT_TEST_RESEAU,
		NOMBRE_CLIENTS_TEST_RESEAU,
		NMAXIMUM_CLIENTS_SIMULTANES_NSERVEUR );
	system( "pause" );

	printf( "\nCreation du serveur...\n" );
	if( !( serveur = NLib_Module_Reseau_Serveur_NServeur_Construire( PORT_TEST_RESEAU,
		receptionPacketServeur,
		connexionServeur,
		deconnexionServeur,
		NULL,
		0 ) ) )
		return NFALSE;

	system( "pause>nul" );

	printf( "Creation des %d clients... ",
		NOMBRE_CLIENTS_TEST_RESEAU );
	for( i = 0; i < NOMBRE_CLIENTS_TEST_RESEAU; i++ )
		if( !( client[ i ] = NLib_Module_Reseau_Client_NClient_Construire( IP_TEST_RESEAU,
			PORT_TEST_RESEAU,
			receptionPacketClient,
			deconnexionClient,
			NULL,
			0 ) ) )
		{
			for( j = 0; j < i; j++ )
				NLib_Module_Reseau_Client_NClient_Detruire( &client[ j ] );
			NLib_Module_Reseau_Serveur_NServeur_Detruire( &serveur );
			return NFALSE;
		}
	printf( "OK\n" );

	printf( "Connexion des client... " );
	for( i = 0; i < NOMBRE_CLIENTS_TEST_RESEAU; i++ )
		if( !NLib_Module_Reseau_Client_NClient_Connecter( client[ i ] ) )
		{
			for( i = 0; i < NOMBRE_CLIENTS_TEST_RESEAU; i++ )
				NLib_Module_Reseau_Client_NClient_Detruire( &client[ i ] );
			NLib_Module_Reseau_Serveur_NServeur_Detruire( &serveur );
			return NFALSE;
		}
	printf( "OK\n" );

	system( "pause>nul" );

	printf( "------------------------------------\n" );
	printf( "Envoi %d packets client->serveur:\n",
		NB_PACKETS_CLIENT_SERVEUR_TEST_RESEAU );
	for( i = 0; i < NB_PACKETS_CLIENT_SERVEUR_TEST_RESEAU; i++ )
	{
		for( j = 0; j < NOMBRE_CLIENTS_TEST_RESEAU; j++ )
		{
			sprintf( buffer,
				"Salut %d id client(%d)",
				i,
				j );

			NLib_Module_Reseau_Client_NClient_AjouterPacket( client[ j ],
				NLib_Module_Reseau_Packet_NPacket_Construire3( buffer,
					strlen( buffer ) ) );

			NLib_Temps_Attendre( 1 );
		}
	}
	printf( "------------------------------------\n" );


	system( "pause>nul" );

	printf( "---------------------------------------\n" );
	printf( "Envoie de %d messages serveur->client:\n",
		NB_PACKETS_SERVEUR_CLIENT_TEST_RESEAU );
	for( i = 0; i < NB_PACKETS_SERVEUR_CLIENT_TEST_RESEAU; i++ )
	{
		for( j = 0; j < NOMBRE_CLIENTS_TEST_RESEAU; j++ )
		{
			sprintf( buffer,
				"%u id(%d) UnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLongUnMessageLong",
				i,
				j );
		
			NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( CLIENTS_TEST_RESEAU[ j ],
				NLib_Module_Reseau_Packet_NPacket_Construire3( buffer,
					strlen( buffer ) + 1 ) );

			NLib_Temps_Attendre( 1 );
		}
	}
	printf( "---------------------------------------\n" );
	system( "pause>nul" );

	printf( "Destruction %d clients...\n",
		NOMBRE_CLIENTS_TEST_RESEAU );
	for( i = 0; i < NOMBRE_CLIENTS_TEST_RESEAU; i++ )
		NLib_Module_Reseau_Client_NClient_Detruire( &client[ i ] );

	printf( "Destruction serveur...\n" );
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &serveur );

	// FIN TEST
	printf( "------------- FIN TEST RESEAU ------------\n" );
	system( "pause>nul" );

	// OK
	return NTRUE;
}
#endif
// FIN TEST RESEAU

// TEST SDL
#ifdef NLIB_MODULE_SDL
typedef struct
{
	NFenetre *fenetre;
	NSaisieSDL *saisie;
	NSurface *texte;
	NPolice *p;
	char *callbackChaine;
} TestSDL;

void actualiserTestSDL( TestSDL *test )
{
	NLib_Module_SDL_NFenetre_Nettoyer( test->fenetre );

	if( test->texte != NULL )
		NLib_Module_SDL_Surface_NSurface_Afficher( test->texte );

	NLib_Module_SDL_NFenetre_Update( test->fenetre );
}

NBOOL actualiserTestSDL2( TestSDL *test )
{
	test->texte = NLib_Module_SDL_TTF_NPolice_CreerTexte( test->p,
		test->fenetre,
		test->callbackChaine );

	return NTRUE;
}

NBOOL testSDL( void )
{
	NFenetre *fenetre;

	NBOOL estContinuer = TRUE;

	SDL_Event *e;

	NSPoint position = { 0, };

	NSurface *s1;

	NU32 ticks = 0;

	SDL_Color color = { 0, };

	NPolice *p;
	NSaisieSDL *s;
	TestSDL ttt;

	NAnimation *animation;

	NU32 idAnimation = 4;

	NSurface *nomBouton;

	NSPoint positionSouris = { 0, };

	NBouton *bouton;

	NTileset *tileset;
	NUPoint positionTileset = { 0, };

	if( !( fenetre = NLib_Module_SDL_NFenetre_Construire( "TEST",
		(NUPoint){ 640, 480 } ) ) )
		return NFALSE;

	if( !( tileset = NLib_Module_SDL_Surface_NTileset_Construire( "Test/Tileset1.png",
		(NUPoint){ 16, 16 },
		fenetre ) ) )
		return NFALSE;

	if( !( s1 = NLib_Module_SDL_Surface_NSurface_Construire2( fenetre,
		"Test/A.png" ) ) )
		return NFALSE;

	if( !( p = NLib_Module_SDL_TTF_NPolice_Construire( "Test/trebuc.ttf",
		18 ) ) )
		return NFALSE;

	NDEFINIR_COULEUR( p->m_couleur,
		0xFF,
		0xFF,
		0xFF );

	if( !( nomBouton = NLib_Module_SDL_TTF_NPolice_CreerTexte( p,
		fenetre,
		"Bouton test" ) ) )
		return NFALSE;

	NLib_Module_SDL_TTF_NPolice_Detruire( &p );

	if( !( p = NLib_Module_SDL_TTF_NPolice_Construire( "Test/trebuc.ttf",
		24 ) ) )
		return NFALSE;
	
	if( !( bouton = NLib_Module_SDL_Bouton_NBouton_Construire( (NSPoint){ 40, 40 },
		(NUPoint){ 100, 40 },
		(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF },
		(NCouleur){ 0x00, 0x00, 0x00, 0xFF },
		fenetre,
		2 ) ) )
		return NFALSE;

	NDEFINIR_COULEUR( p->m_couleur,
		255,
		255,
		255 );

	if( !( s = NLib_Module_SDL_Saisie_NSaisieSDL_Construire( NMODE_SAISIE_SDL_TEXTE_TOTAL,
		0,
		SDLK_RETURN,
		NULL,
		actualiserTestSDL,
		actualiserTestSDL2,
		NULL,
		&ttt,
		&ttt,
		NULL ) ) )
		return NFALSE;

	ttt.fenetre = fenetre;
	ttt.saisie = s;
	ttt.texte = NULL;
	ttt.p = p;
	ttt.callbackChaine = NULL;

	NLib_Module_SDL_Surface_NSurface_DefinirPosition( s1,
		30,
		30 );

	if( !( animation = NLib_Module_SDL_Surface_NAnimation_Construire( "Test/C1.png",
		(NUPoint){ 21,30 },
		150,
		fenetre ) ) )
		return NFALSE;

	while( estContinuer )
	{
		if( SDL_GetTicks( ) - ticks >= 1000 )
		{
			ticks = SDL_GetTicks( );

			NDEFINIR_COULEUR_A( color,
				NLib_Temps_ObtenirNombreAleatoire( )%0xFF,
				NLib_Temps_ObtenirNombreAleatoire( )%0xFF,
				NLib_Temps_ObtenirNombreAleatoire( )%0xFF,
				0xFF );
		}

		// Nettoyer
		NLib_Module_SDL_NFenetre_Nettoyer2( fenetre,
			color.r,
			color.g,
			color.b,
			color.a );

		// R�cup�rer �venement
		e = NLib_Module_SDL_NFenetre_ObtenirEvenement( fenetre );

		// Lire �venement
		SDL_PollEvent( e );

		// Analyser evenement
		switch( e->type )
		{
			case SDL_QUIT:
				estContinuer = NFALSE;
				break;

			case SDL_KEYDOWN:
				switch( e->key.keysym.sym )
				{
					case SDLK_a:
						printf( "a" );
						break;

						/*
					case SDLK_UP:
						if( positionTileset.y > 0 )
							positionTileset.y--;
						break;
					case SDLK_DOWN:
						if( positionTileset.y < tileset->m_nombreCase.y - 1 )
							positionTileset.y++;
						break;
					case SDLK_LEFT:
						if( positionTileset.x > 0 )
							positionTileset.x--;
						break;
					case SDLK_RIGHT:
						if( positionTileset.x < tileset->m_nombreCase.x - 1 )
							positionTileset.x++;
						break;*/

					case SDLK_UP:
						if( position.y > 0 )
						position.y--;
						idAnimation = 0;
						break;
					case SDLK_DOWN:
						position.y++;
						idAnimation = 1;
						break;
					case SDLK_LEFT:
						if( position.x > 0 )
						position.x--;
						idAnimation = 2;
						break;
					case SDLK_RIGHT:
						position.x++;
						idAnimation = 3;
						break;

					case SDLK_w:
						idAnimation = 0;
						break;
					case SDLK_x:
						idAnimation = 1;
						break;
					case SDLK_c:
						idAnimation = 2;
						break;
					case SDLK_v:
						idAnimation = 3;
						break;

					case SDLK_KP_1:
					case SDLK_KP_2:
					case SDLK_KP_3:
					case SDLK_KP_4:
					case SDLK_KP_5:
					case SDLK_KP_6:
					case SDLK_KP_7:
					case SDLK_KP_8:
					case SDLK_KP_9:
						NLib_Module_SDL_Surface_NSurface_DefinirZoom( s1,
							9 - ( SDLK_KP_9 - e->key.keysym.sym ) );
						break;

					

					case SDLK_s:
						NLib_Module_SDL_Saisie_NSaisieSDL_Saisir( s,
							e,
							NULL,
							NFALSE,
							&ttt.callbackChaine );
						break;

					default:
						printf( "He?" );
						break;
				}
				break;

			case SDL_MOUSEMOTION:
				NLib_Module_SDL_Surface_NSurface_DefinirPosition( s1,
					e->motion.x - s1->m_taille.x / 2,
					e->motion.y - s1->m_taille.y / 2 );

				NDEFINIR_POSITION( positionSouris,
					e->motion.x,
					e->motion.y );
				break;

			default:
				break;
		}

		NLib_Module_SDL_Surface_NSurface_Afficher( s1 );

		// Bouton
		switch( bouton->m_etat )
		{
			case NETAT_BOUTON_SURVOLE:
				NDEFINIR_COULEUR_A( bouton->m_cadre->m_couleurFond,
					0xFF,
					0x00,
					0x00,
					0x7F );
				NDEFINIR_COULEUR( bouton->m_cadre->m_couleur,
					0x00,
					0xFF,
					0x00 );
				NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( nomBouton,
					0x00,
					0x00,
					0xFF );
				break;
			default:
			case NETAT_BOUTON_REPOS:
				NDEFINIR_COULEUR_A( bouton->m_cadre->m_couleurFond,
					0x00,
					0x00,
					0x00,
					0xFF );
				NDEFINIR_COULEUR( bouton->m_cadre->m_couleur,
					0xFF,
					0xFF,
					0xFF );
				NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( nomBouton,
					0xFF,
					0xFF,
					0xFF );
				break;
			case NETAT_BOUTON_PRESSE:
				NDEFINIR_COULEUR_A( bouton->m_cadre->m_couleurFond,
					0x00,
					0xFF,
					0x00,
					0x7F );
				NDEFINIR_COULEUR( bouton->m_cadre->m_couleur,
					0xFF,
					0x00,
					0x00 );
				NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( nomBouton,
					0x00,
					0xAA,
					0xAA );
				break;
		}

		NLib_Module_SDL_NCadre_Deplacer( bouton->m_cadre,
			positionSouris,
			bouton->m_etat == NETAT_BOUTON_PRESSE );

		NLib_Module_SDL_Bouton_NBouton_Dessiner( bouton );

		NDEFINIR_POSITION( nomBouton->m_position,
			bouton->m_cadre->m_position.x+ ( bouton->m_cadre->m_taille.x / 2 - nomBouton->m_taille.x / 2 ),
			bouton->m_cadre->m_position.y + ( bouton->m_cadre->m_taille.y / 2 - nomBouton->m_taille.y / 2 ) );
		NLib_Module_SDL_Bouton_NBouton_Update( bouton,
			&positionSouris,
			e );
		NLib_Module_SDL_Surface_NSurface_Afficher( nomBouton );

		if( ttt.texte != NULL )
			NLib_Module_SDL_Surface_NSurface_Afficher( ttt.texte );

		NLib_Module_SDL_Surface_NSurface_DefinirPosition( (NSurface*)NLib_Module_SDL_Surface_NTileset_ObtenirCase( tileset, positionTileset ),
			70,
			70 );
		NLib_Module_SDL_Surface_NSurface_Afficher( NLib_Module_SDL_Surface_NTileset_ObtenirCase( tileset, positionTileset ) );

		NLib_Module_SDL_Surface_NAnimation_DefinirPosition( animation,
			position );
		NLib_Module_SDL_Surface_NAnimation_Update( animation );
		NLib_Module_SDL_Surface_NAnimation_Afficher( animation,
			idAnimation );
		// Actualiser
		NLib_Module_SDL_NFenetre_Update( fenetre );
	}

	NLib_Module_SDL_Surface_NAnimation_Detruire( &animation );
	NLib_Module_SDL_Surface_NTileset_Detruire( &tileset );
	NLib_Module_SDL_Surface_NSurface_Detruire( &s1 );
	NLib_Module_SDL_Surface_NSurface_Detruire( &ttt.texte );

	NLib_Module_SDL_NFenetre_Detruire( &fenetre );

	NLib_Module_SDL_TTF_NPolice_Detruire( &p );
	NLib_Module_SDL_Bouton_NBouton_Detruire( &bouton );
	NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &s );
	

	// OK
	return NTRUE;
}
#endif // NLIB_MODULE_SDL
// FIN TEST SDL

// TEST REPERTOIRE
#ifdef NLIB_MODULE_REPERTOIRE
NBOOL testRepertoire( void )
{
#define REPERTOIRE_TEST		"Test"
	NRepertoire *repertoire;

	NU32 i;

	// Cr�er
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
		return NFALSE;

	// Lister le contenu de "Test"
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		REPERTOIRE_TEST"/*",
		NATTRIBUT_REPERTOIRE_NORMAL ) )
		return NFALSE;

	printf( "Contenu de %s contenant %d fichier(s):\n",
		REPERTOIRE_TEST,
		NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ) );

	// Lister
	for( i = 0; i < NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ); i++ )
		printf( "[%d] %s [%d octets]\n",
			i,
			NLib_Module_Repertoire_NRepertoire_ObtenirFichier( repertoire,
				i )->m_nom,
			NLib_Module_Repertoire_NRepertoire_ObtenirFichier( repertoire,
				i )->m_taille );
	// Lib�rer
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// OK
	return NTRUE;
}
#endif // NLIB_MODULE_REPERTOIRE
// FIN TEST REPERTOIRE

// TEST FMODEX
#ifdef NLIB_MODULE_FMODEX
NBOOL testFModex( void )
{
	NMusique *musique;
	NSon *son;

	char k;

	printf( "TEST FMODEX:\n" );

	printf( "Creation musique et son... " );
	if( !( musique = NLib_Module_FModex_NMusique_Construire( "Test/K.mp3",
		100,
		NTRUE ) ) )
		return NFALSE;
	if( !( son = NLib_Module_FModex_NSon_Construire( "Test/S.mp3",
		100 ) ) )
	{
		NLib_Module_FModex_NMusique_Detruire( &musique );
		return NFALSE;
	}
	printf( "OK\n" );


	printf( "p pour alterner lecture/pause de la musique,\n" );
	printf( "e pour lancer un effet,\n" );
	printf( "s pour quitter le test.\n" );
	do
	{
		// Lire caract�re
		k = (char)getchar( );

		// Analyser caract�re
		switch( k )
		{
			case 'p':
				// Inverser �tat lecture
				if( NLib_Module_FModex_NMusique_EstLecture( musique ) )
					NLib_Module_FModex_NMusique_Pause( musique );
				else
					NLib_Module_FModex_NMusique_Lire( musique );
				break;

			case 'e':
				// Lancer effet
				NLib_Module_FModex_NSon_Lire( son );
				break;
		}
	} while( k != 's' );

	// D�truire
	NLib_Module_FModex_NMusique_Detruire( &musique );
	NLib_Module_FModex_NSon_Detruire( &son );

	// OK
	return NTRUE;
}
#endif // NLIB_MODULE_FMODEX
// FIN TEST FMODEX

NBOOL testFichierTexte( void )
{
	NFichierTexte *f;

	printf( "Test fichier:\n" );

	if( !( f = NLib_Fichier_NFichierTexte_ConstruireLecture( "Test/Fichier.txt" ) ) )
		return NFALSE;

	// Lire
	if( !NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( f,
		"PK8:" ) )
	{
		NLib_Fichier_NFichierTexte_Detruire( &f );
		return NFALSE;
	}

	printf( "%d\n", NLib_Fichier_NFichierTexte_LireNombre( f, NTRUE, NFALSE ) );
	printf( "%d\n", NLib_Fichier_NFichierTexte_LireNombre( f, NTRUE, NFALSE ) );

	NLib_Fichier_NFichierTexte_Detruire( &f );

	printf( "-------\n\n" );

	return NTRUE;
}

NBOOL testProjet( void )
{
	BRessource *r;

	NFenetre *fenetre;

	if( !( fenetre = NLib_Module_SDL_NFenetre_Construire( "Projet",
		(NUPoint){ 640, 480 } ) ) )
		return NFALSE;

	if( !( r = Projet_Commun_Ressource_BRessource_Construire( fenetre ) ) )
		return NFALSE;

	Projet_Commun_Ressource_BRessource_Detruire( &r );

	NLib_Module_SDL_NFenetre_Detruire( &fenetre );

	// OK
	return NTRUE;
}

NS32 Projet_Test_Main( void )
{
	// R�sultat test
	NU32 nombreTestsReussis = 0;
	
	// Comptabilisation tests
	NU32 nombreTestsEffectues = 0;

	// Code de retour du test
	NBOOL codeTest;

	NLib_Initialiser( Projet_Commun_Erreur_CallbackNotificationErreur );

#define TEST( fct, \
	nomTest ) \
	{ \
		nombreTestsEffectues++; \
 \
		codeTest = fct( ); \
 \
		printf( "Le test %s a [%s].\n", nomTest, codeTest ? "REUSSI" : "ECHOUE" ); \
 \
		nombreTestsReussis += codeTest ? 1 : 0; \
	}

	// Test �l�ments projet
	TEST( testProjet,
		"projet" );
	// Fin test �l�ments projet

	// Lecture fichier test
	TEST( testFichierTexte,
		"fichier" );
	// Fin lecture fichier test

#ifdef NLIB_MODULE_FMODEX
	TEST( testFModex,
		"FModex" );
#endif // NLIB_MODULE_FMODEX

#ifdef NLIB_MODULE_REPERTOIRE
	TEST( testRepertoire,
		"repertoire" );
#endif // NLIB_MODULE_SDL

#ifdef NLIB_MODULE_SDL
	TEST( testSDL,
		"SDL" );
#endif // NLIB_MODULE_SDL

	// TEST RESEAU
#ifdef NLIB_MODULE_RESEAU
	TEST( test_reseau,
		"reseau" );
#endif // NLIB_MODULE_RESEAU
	// FIN TEST RESEAU

	NLib_Detruire( );

	// Notifier la reussite des tests
	if( !nombreTestsEffectues )
		printf( "Aucun test effectue.\n" );
	else
		printf( "%d / %d test(s) effectue(s) avec succes [%1.2f%%].\n",
			nombreTestsReussis,
			nombreTestsEffectues,
			( (double)nombreTestsReussis / (double)nombreTestsEffectues ) * 100.0 );

	// OK
	return EXIT_SUCCESS;
}

