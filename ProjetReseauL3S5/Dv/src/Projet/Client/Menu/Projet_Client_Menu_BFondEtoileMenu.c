#include "../../../../include/Projet/Projet.h"

// --------------------------------------------
// struct Projet::Client::Menu::BFondEtoileMenu
// --------------------------------------------

/* Est d�j� �toile pr�sente? (priv�e) */
NBOOL Projet_Client_Menu_BFondEtoileMenu_EstDejaPresenteEtoileInterne( BFondEtoileMenu *this,
	NSPoint position,
	NU32 etoileActuelle )
{
	// It�rateur
	NU32 i = 0;

	// V�rifier
	for( ; i < etoileActuelle; i++ )
		if( position.x == this->m_etoile[ i ].x
			&& position.y == this->m_etoile[ i ].y )
			return NTRUE;

	// Pas de correspondance
	return NFALSE;
}

/* Construire */
__ALLOC BFondEtoileMenu *Projet_Client_Menu_BFondEtoileMenu_Construire( const NFenetre *fenetre,
	NU32 delaiEntreUpdate,
	NCouleur couleur )
{
	// Sortie
	__OUTPUT BFondEtoileMenu *out;

	// Position
	NSPoint position;

	// It�rateur
	NU32 i;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BFondEtoileMenu ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_fenetre = fenetre;
	out->m_couleur = couleur;
	out->m_delaiEntreUpdate = delaiEntreUpdate;

	// Placer les �toiles
	for( i = 0; i < BNOMBRE_ETOILES_MENU; i++ )
	{
		// Chercher emplacement libre
		do
		{
			// D�finir al�atoire
			NDEFINIR_POSITION( position,
				( NLib_Temps_ObtenirNombreAleatoire( ) % NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->x ),
				( NLib_Temps_ObtenirNombreAleatoire( ) % NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->y ) );
		} while( Projet_Client_Menu_BFondEtoileMenu_EstDejaPresenteEtoileInterne( out,
			position,
			i ) );

		// Enregistrer la position
		NDEFINIR_POSITION( out->m_etoile[ i ],
			position.x,
			position.y );
	}

	// OK
	return out;
}

/* D�truire */
void Projet_Client_Menu_BFondEtoileMenu_Detruire( BFondEtoileMenu **this )
{
	NFREE( *this );
}

/* Afficher */
void Projet_Client_Menu_BFondEtoileMenu_Afficher( const BFondEtoileMenu *this )
{
	// Afficher les �toiles
	NLib_Module_SDL_NFenetre_DessinerPoints( (NFenetre*)this->m_fenetre,
		((BFondEtoileMenu*)this)->m_etoileAffichee,
		BNOMBRE_ETOILES_MENU,
		this->m_couleur );
}

/* Update */
void Projet_Client_Menu_BFondEtoileMenu_Update( BFondEtoileMenu *this )
{
	// It�rateur
	NU32 i;

	// V�rifier timer
	if( NLib_Temps_ObtenirTick( ) - this->m_tempsDernierUpdate < this->m_delaiEntreUpdate )
		return;

	// Mettre � jour le scrolling �toiles
		// x
			if( this->m_scrollEtoile.x < (NS32)NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x )
				this->m_scrollEtoile.x++;
			else
				this->m_scrollEtoile.x = 0;
		// y
			if( this->m_scrollEtoile.y < (NS32)NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y )
				this->m_scrollEtoile.y++;
			else
				this->m_scrollEtoile.y = 0;
	

	// Mettre � jour les positions des �toiles
	for( i = 0; i < BNOMBRE_ETOILES_MENU; i++ )
		NDEFINIR_POSITION( this->m_etoileAffichee[ i ],
			( this->m_etoile[ i ].x + this->m_scrollEtoile.x ) % NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x,
			( this->m_etoile[ i ].y + this->m_scrollEtoile.y ) % NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y );

	// Enregistrer dernier temps
	this->m_tempsDernierUpdate = NLib_Temps_ObtenirTick( );
}

