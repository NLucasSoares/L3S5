#define PROJET_CLIENT_CONFIGURATION_BLISTEPROPRIETECONFIGURATION_INTERNE
#include "../../../../include/Projet/Projet.h"

// ----------------------------------------------------------------
// enum Projet::Client::Configuration::BListeProprieteConfiguration
// ----------------------------------------------------------------

/* Composer l'ensemble des clefs */
__ALLOC char **Projet_Client_Configuration_BListeProprieteConfiguration_ObtenirEnsembleClef( void )
{
	// Sortie
	__OUTPUT char **out;

	// It�rateurs
	NU32 i,
		j;

	// Allouer la m�moire
	if( !( out = calloc( BLISTE_PROPRIETES_CONFIGURATION,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	for( i = 0; i < BLISTE_PROPRIETES_CONFIGURATION; i++ )
	{
		// Allouer la m�moire
		if( !( out[ i ] = calloc( strlen( BListeProprieteConfigurationTexte[ i ] ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Lib�rer
			for( j = 0; j < i; j++ )
				NFREE( out[ j ] );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Copier
		memcpy( out[ i ],
			BListeProprieteConfigurationTexte[ i ],
			strlen( BListeProprieteConfigurationTexte[ i ] ) );
	}

	// OK
	return out;
}

