#include "../../../../../include/Projet/Projet.h"

// -----------------------------------------------
// struct Projet::Commun::Carte::Tileset::BTileset
// -----------------------------------------------

/* Lire l'ordre d'animation (priv�e) */
__ALLOC NU32 *Projet_Commun_Carte_Tileset_BTileset_LireOrdreAnimationInterne( const char *ordre,
	NU32 nombreOrdre )
{
	// Sortie
	__OUTPUT NU32 *out;

	// Curseur
	NU32 curseur = 0;

	// Buffer
	char *buffer;

	// It�rateur
	NU32 i = 0;

	// Allouer la m�moire
	if( !( out = calloc( nombreOrdre,
		sizeof( NU32 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	for( ; i < nombreOrdre; i++ )
	{
		// Lire la frame
		if( !( buffer = NLib_Chaine_LireEntre2( ordre,
			'"',
			&curseur,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

			// Lib�rer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Enregistrer
		out[ i ] = strtol( buffer,
			NULL,
			10 );

		// Lib�rer
		NFREE( buffer );
	}

	// OK
	return out;
}

/* Construire */
__ALLOC BTileset *Projet_Commun_Carte_Tileset_BTileset_Construire( const char *lienDefinition,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BTileset *out;

	// It�rateur
	NU32 i;

	// Fichier
	NFichierClef *fichier;

	// Lien
	char lien[ MAX_PATH ];

	// Clefs
	char **clefs;

	// Ordre animation
	NU32 *ordre;

	// Obtenir l'ensemble des clefs
	if( !( clefs = Projet_Commun_Fichier_Tileset_BClefFichierTileset_ComposerEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lienDefinition,
		clefs,
		BCLEFS_FICHIER_TILESET ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		for( i = 0; i < BCLEFS_FICHIER_TILESET; i++ )
			NFREE( clefs[ i ] );
		NFREE( clefs );

		// Quitter
		return NULL;
	}
	
	// Lib�rer les clefs
	for( i = 0; i < BCLEFS_FICHIER_TILESET; i++ )
		NFREE( clefs[ i ] );
	NFREE( clefs );

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BTileset ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// D�truire le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Lire si anim�
	out->m_estAnime = !_strcmpi( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
			BCLEF_FICHIER_TILESET_EST_ANIME ),
		"Oui" );

	// Composer le lien
	sprintf( lien,
		"%s/%s.%s",
		BREPERTOIRE_TILESET,
		NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
			BCLEF_FICHIER_TILESET_NOM ),
		BEXTENSION_FICHIER_IMAGE );

	// Traiter selon le type
	switch( out->m_estAnime )
	{
		default:
		case NFALSE:
			// Cr�er le tileset
			if( !( out->m_tileset = NLib_Module_SDL_Surface_NTileset_Construire( lien,
				BTAILLE_CASE_TILESET,
				fenetre ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// D�truire le fichier
				NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

				// Lib�rer
				NFREE( out );

				// Quitter
				return NULL;
			}
			break;

		case NTRUE:
			// Lire l'ordre d'animation
			if( !( ordre = Projet_Commun_Carte_Tileset_BTileset_LireOrdreAnimationInterne( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
					BCLEF_FICHIER_TILESET_ORDRE_ANIMATION ),
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					BCLEF_FICHIER_TILESET_TAILLE_ORDRE_ANIMATION ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// D�truire le fichier
				NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

				// Lib�rer
				NFREE( out );

				// Quitter
				return NULL;
			}

			// Construire l'animation
			if( !( out->m_animation = NLib_Module_SDL_Surface_NAnimation_Construire2( lien,
				BTAILLE_CASE_TILESET,
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					BCLEF_FICHIER_TILESET_TEMPS_CHANGEMENT_ANIMATION ),
				fenetre,
				ordre,
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					BCLEF_FICHIER_TILESET_TAILLE_ORDRE_ANIMATION ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// D�truire l'ordre
				NFREE( ordre );

				// D�truire le fichier
				NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

				// Lib�rer
				NFREE( out );

				// Quitter
				return NULL;
			}

			// Lib�rer l'ordre
			NFREE( ordre );
			break;
	}

	// D�truire le fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Carte_Tileset_BTileset_Detruire( BTileset **this )
{
	// D�truire
	if( (*this)->m_estAnime )
		NLib_Module_SDL_Surface_NAnimation_Detruire( &(*this)->m_animation );
	else
		NLib_Module_SDL_Surface_NTileset_Detruire( &(*this)->m_tileset );

	// Lib�rer
	NFREE( *this );
}

/* Update */
void Projet_Commun_Carte_Tileset_BTileset_Update( BTileset *this )
{
	// V�rifier si il s'agit d'un tileset anim�
	if( !this->m_estAnime )
		return;

	// Mettre � jour
	NLib_Module_SDL_Surface_NAnimation_Update( this->m_animation );
}

/* Est anim�? */
NBOOL Projet_Commun_Carte_Tileset_BTileset_EstAnime( const BTileset *this )
{
	return this->m_estAnime;
}

/* Obtenir nombre cases */
NUPoint Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( const BTileset *this )
{
	// Sortie
	__OUTPUT NUPoint out;

	if( this->m_estAnime )
	{
		// D�finir
		NDEFINIR_POSITION( out,
			1,
			NLib_Module_SDL_Surface_NAnimation_ObtenirNombreAnimation( this->m_animation ) );

		// OK
		return out;
	}
	else
		return NLib_Module_SDL_Surface_NTileset_ObtenirNombreCase( this->m_tileset );
}

/* Afficher */
void Projet_Commun_Carte_Tileset_BTileset_Afficher( const BTileset *this,
	NUPoint caseAAfficher,
	NSPoint position,
	NU32 zoom )
{
	// Surface
	NSurface *s;

	// Si le tileset est anim�
	if( this->m_estAnime )
	{
		// D�finir la position
		NLib_Module_SDL_Surface_NAnimation_DefinirPosition( this->m_animation,
			position );

		// Afficher
		NLib_Module_SDL_Surface_NAnimation_Afficher2( this->m_animation,
			caseAAfficher.y,
			zoom );
	}
	// Si le tileset est fixe
	else
	{
		// Surface
		s = (NSurface*)NLib_Module_SDL_Surface_NTileset_ObtenirCase( this->m_tileset,
			caseAAfficher );

		// D�finir la position
		NLib_Module_SDL_Surface_NSurface_DefinirPosition2( s,
			position );

		// D�finir le zoom
		NLib_Module_SDL_Surface_NSurface_DefinirZoom( s,
			zoom );

		// Afficher
		NLib_Module_SDL_Surface_NSurface_Afficher( s );
	}
}

