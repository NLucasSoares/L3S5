#include "../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// --------------------------------------------
// struct Projet::Commun::Ressource::BRessource
// --------------------------------------------

/* Calculer le checksum des ressources (priv�e) */
NU32 Projet_Commun_Ressource_BRessource_CalculerChecksumInterne( const BRessource *this )
{
	// Sortie
	__OUTPUT NU32 out = 0;
	
	// It�rateur
	NU32 i;

	// Calculer
		// Nombre personnages
			out += this->m_nombrePersonnage;
		// Personnages
			for( i = 0; i < this->m_nombrePersonnage; i++ )
				out += Projet_Commun_Personnage_BPersonnage_ObtenirVitesse( this->m_personnage[ i ] )
					* Projet_Commun_Personnage_BPersonnage_ObtenirTauxDrop( this->m_personnage[ i ] )
					* strlen( Projet_Commun_Personnage_BPersonnage_ObtenirNom( this->m_personnage[ i ] ) );
		// Nombre tilesets
			out += this->m_nombreTileset;
		// Tilesets
			for( i = 0; i < this->m_nombreTileset; i++ )
				out += Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( this->m_tileset[ i ] ).x * Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( this->m_tileset[ i ] ).y;
		// Puissance minimale/maximale
			out += ( BPUISSANCE_INITIALE_JOUEUR + 1 ) * ( BPUISSANCE_MAXIMALE_JOUEUR + 2 );

	// OK
	return out;
}

/* Construire */
__ALLOC BRessource *Projet_Commun_Ressource_BRessource_Construire( const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BRessource *out;

	// It�rateur
	NU32 i;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BRessource ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Charger les personnages
	if( !( out->m_personnage = Projet_Commun_Personnage_Chargement_Charger( &out->m_nombrePersonnage,
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger les tilesets
	if( !( out->m_tileset = Projet_Commun_Carte_Tileset_Chargement_Charger( &out->m_nombreTileset,
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		for( i = 0; i < out->m_nombrePersonnage; i++ )
			Projet_Commun_Personnage_BPersonnage_Detruire( &out->m_personnage[ i ] );
		NFREE( out->m_personnage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger les musiques
	if( !( out->m_musique = Projet_Commun_Ressource_Audio_Musique_BMusique_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		for( i = 0; i < out->m_nombreTileset; i++ )
			Projet_Commun_Carte_Tileset_BTileset_Detruire( &out->m_tileset[ i ] );
		NFREE( out->m_tileset );
		for( i = 0; i < out->m_nombrePersonnage; i++ )
			Projet_Commun_Personnage_BPersonnage_Detruire( &out->m_personnage[ i ] );
		NFREE( out->m_personnage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger les animations
	if( !( out->m_animation = Projet_Commun_Ressource_Animation_BEnsembleAnimation_Construire( fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		Projet_Commun_Ressource_Audio_Musique_BMusique_Detruire( &out->m_musique );
		for( i = 0; i < out->m_nombreTileset; i++ )
			Projet_Commun_Carte_Tileset_BTileset_Detruire( &out->m_tileset[ i ] );
		NFREE( out->m_tileset );
		for( i = 0; i < out->m_nombrePersonnage; i++ )
			Projet_Commun_Personnage_BPersonnage_Detruire( &out->m_personnage[ i ] );
		NFREE( out->m_personnage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger bonus
	if( !( out->m_bonus = Projet_Commun_Ressource_Bonus_BRessourceBonus_Construire( BLIEN_RESSOURCE_ICONE_BONUS,
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		Projet_Commun_Ressource_Animation_BEnsembleAnimation_Detruire( &out->m_animation );
		Projet_Commun_Ressource_Audio_Musique_BMusique_Detruire( &out->m_musique );
		for( i = 0; i < out->m_nombreTileset; i++ )
			Projet_Commun_Carte_Tileset_BTileset_Detruire( &out->m_tileset[ i ] );
		NFREE( out->m_tileset );
		for( i = 0; i < out->m_nombrePersonnage; i++ )
			Projet_Commun_Personnage_BPersonnage_Detruire( &out->m_personnage[ i ] );
		NFREE( out->m_personnage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger effet sonore
	if( !( out->m_effetSonore = Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Detruire( &out->m_effetSonore );
		Projet_Commun_Ressource_Animation_BEnsembleAnimation_Detruire( &out->m_animation );
		Projet_Commun_Ressource_Audio_Musique_BMusique_Detruire( &out->m_musique );
		for( i = 0; i < out->m_nombreTileset; i++ )
			Projet_Commun_Carte_Tileset_BTileset_Detruire( &out->m_tileset[ i ] );
		NFREE( out->m_tileset );
		for( i = 0; i < out->m_nombrePersonnage; i++ )
			Projet_Commun_Personnage_BPersonnage_Detruire( &out->m_personnage[ i ] );
		NFREE( out->m_personnage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Calculer les checksum des ressources
	out->m_checksum = Projet_Commun_Ressource_BRessource_CalculerChecksumInterne( out );

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Ressource_BRessource_Detruire( BRessource **this )
{
	// It�rateur
	NU32 i = 0;

	// D�truire effet sonore
	Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Detruire( &(*this)->m_effetSonore );

	// D�truire bonus
	Projet_Commun_Ressource_Bonus_BRessourceBonus_Detruire( &(*this)->m_bonus );

	// D�truire les animations
	Projet_Commun_Ressource_Animation_BEnsembleAnimation_Detruire( &(*this)->m_animation );

	// D�truire les musiques
	Projet_Commun_Ressource_Audio_Musique_BMusique_Detruire( &(*this)->m_musique );

	// D�truire les personnages
	for( ; i < (*this)->m_nombrePersonnage; i++ )
		Projet_Commun_Personnage_BPersonnage_Detruire( &(*this)->m_personnage[ i ] );

	// Lib�rer le conteneur personnages
	NFREE( (*this)->m_personnage );

	// D�truire les tilesets
	for( i = 0; i < (*this)->m_nombreTileset; i++ )
		Projet_Commun_Carte_Tileset_BTileset_Detruire( &(*this)->m_tileset[ i ] );

	// Lib�rer le conteneur tileset
	NFREE( (*this)->m_tileset );

	// Lib�rer
	NFREE( (*this) );
}

/* Obtenir le nombre de personnages */
NU32 Projet_Commun_Ressource_BRessource_ObtenirNombrePersonnage( const BRessource *this )
{
	return this->m_nombrePersonnage;
}

/* Obtenir un personnage */
BPersonnage *Projet_Commun_Ressource_BRessource_ObtenirPersonnage( const BRessource *this,
	NU32 personnage )
{
	// V�rifier
	if( personnage >= this->m_nombrePersonnage )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// OK
	return this->m_personnage[ personnage ];
}

BPersonnage *Projet_Commun_Ressource_BRessource_ObtenirPersonnage2( const BRessource *this,
	const char *nom )
{
	// It�rateur
	NU32 i = 0;

	// Chercher
	for( ; i < this->m_nombrePersonnage; i++ )
		if( !_strcmpi( Projet_Commun_Personnage_BPersonnage_ObtenirNom( this->m_personnage[ i ] ),
			nom ) )
			return this->m_personnage[ i ];

	// Introuvable
	return NULL;
}

/* Obtenir l'ensemble des personnages */
BPersonnage **Projet_Commun_Ressource_BRessource_ObtenirEnsemblePersonnage( const BRessource *this )
{
	return this->m_personnage;
}

/* Obtenir ensemble animations */
const BEnsembleAnimation *Projet_Commun_Ressource_BRessource_ObtenirEnsembleAnimation( const BRessource *this )
{
	return this->m_animation;
}

/* Obtenir ic�ne bonus */
const NSurface *Projet_Commun_Ressource_BRessource_ObtenirIconeBonus( const BRessource *this,
	BListeBonus bonus )
{
	return Projet_Commun_Ressource_Bonus_BRessourceBonus_Obtenir( this->m_bonus,
		bonus );
}

/* Obtenir bonus */
const BRessourceBonus *Projet_Commun_Ressource_BRessource_ObtenirBonus( const BRessource *this )
{
	return this->m_bonus;
}

/* Lire musique */
NBOOL Projet_Commun_Ressource_BRessource_LireMusique( const BRessource *this,
	BListeMusique musique )
{
	return Projet_Commun_Ressource_Audio_Musique_BMusique_Lire( this->m_musique,
		musique );
}

/* Lire effet sonore */
void Projet_Commun_Ressource_BRessource_LireEffetSonore( const BRessource *this,
	BListeEffetSonore effet )
{
	Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Lire( this->m_effetSonore,
		effet );
}

/* Est lecture musique en cours? */
NBOOL Projet_Commun_Ressource_BRessource_EstLectureMusique( const BRessource *this )
{
	return Projet_Commun_Ressource_Audio_Musique_BMusique_EstLecture( this->m_musique );
}

/* Arr�ter musique */
NBOOL Projet_Commun_Ressource_BRessource_ArreterMusique( const BRessource *this )
{
	return Projet_Commun_Ressource_Audio_Musique_BMusique_Arreter( this->m_musique );
}

/* Update */
void Projet_Commun_Ressource_BRessource_Update( BRessource *this )
{
	// It�rateur
	NU32 i;

	// Mettre � jour les animations
	Projet_Commun_Ressource_Animation_BEnsembleAnimation_Update( this->m_animation );

	// Mettre � jour les personnages
	for( i = 0; i < this->m_nombrePersonnage; i++ )
		Projet_Commun_Personnage_BPersonnage_Update( this->m_personnage[ i ] );

	// Mettre � jour les tilesets
	for( i = 0; i < this->m_nombreTileset; i++ )
		Projet_Commun_Carte_Tileset_BTileset_Update( this->m_tileset[ i ] );
}

/* Obtenir le nombre de tileset */
NU32 Projet_Commun_Ressource_BRessource_ObtenirNombreTileset( const BRessource *this )
{
	return this->m_nombreTileset;
}

/* Obtenir les tilesets */
const BTileset **Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( const BRessource *this )
{
	return this->m_tileset;
}

/* Obtenir un tileset */
BTileset *Projet_Commun_Ressource_BRessource_ObtenirTileset( const BRessource *this,
	NU32 id )
{
	// V�rifier
	if( id >= this->m_nombreTileset )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// OK
	return this->m_tileset[ id ];
}

/* Obtenir checksum */
NU32 Projet_Commun_Ressource_BRessource_ObtenirChecksum( const BRessource *this )
{
	return this->m_checksum;
}

