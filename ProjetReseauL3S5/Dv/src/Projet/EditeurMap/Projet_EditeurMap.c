#include "../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ----------------------------
// namespace Projet::EditeurMap
// ----------------------------

/* Main �diteur map */
NS32 Projet_EditeurMap_Main( void )
{
	// Editeur
	BEditeur *editeur;

	// Initialiser NLib
	if( !NLib_Initialiser( Projet_Commun_Erreur_CallbackNotificationErreur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Construire l'�diteur
	if( !( editeur = Projet_EditeurMap_BEditeur_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Editer
	Projet_EditeurMap_BEditeur_Editer( editeur );

	// D�truire l'�diteur
	Projet_EditeurMap_BEditeur_Detruire( &editeur );

	// D�truire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

