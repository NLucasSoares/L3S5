#ifndef NLIB_MUTEX_NMUTEX_PROTECT
#define NLIB_MUTEX_NMUTEX_PROTECT

// --------------------------
// struct NLib::Mutex::NMutex
// --------------------------

#ifdef IS_WINDOWS
#include <WinSock2.h>
#endif // IS_WINDOWS

typedef struct
{
#ifdef IS_WINDOWS
	HANDLE m_mutex;
#else // IS_WINDOWS
#error Creer mutex.
#endif // !IS_WINDOWS
} NMutex;

/* Construire */
__ALLOC NMutex *NLib_Mutex_NMutex_Construire( void );

/* D�truire */
void NLib_Mutex_NMutex_Detruire( NMutex** );

/* Lock */
NBOOL NLib_Mutex_NMutex_Lock( NMutex* );

/* Unlock */
void NLib_Mutex_NMutex_Unlock( NMutex* );

#endif // !NLIB_MUTEX_NMUTEX_PROTECT

