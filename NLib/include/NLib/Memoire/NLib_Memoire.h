#ifndef NLIB_MEMOIRE_PROTECT
#define NLIB_MEMOIRE_PROTECT

/*
	Gestion de la m�moire

	@author SOARES Lucas
*/

// -----------------------
// namespace NLib::Memoire
// -----------------------

/* R�allouer m�moire */
NBOOL NLib_Memoire_ReallouerMemoire( char **src,
	NU32 tailleSrc,
	NS32 delta );

/* Ajout donn�es */
NBOOL NLib_Memoire_AjouterData( char **mem,
	NU32 tailleMem,
	const char *memAjout,
	NU32 tailleMemAjout );

/* Calculer CRC32 */
NU32 NLib_Memoire_CalculerCRC( const char *data,
	NU32 taille );

/* Swap */
NBOOL NLib_Memoire_Swap( void *d1,
	void *d2,
	NU32 taille );
void NLib_Memoire_SwapEntier( NU32 *d1,
	NU32 *d2 );

#endif // !NLIB_MEMOIRE_PROTECT

