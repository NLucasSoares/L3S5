#ifndef NLIB_ERREUR_NERREUR_PROTECT
#define NLIB_ERREUR_NERREUR_PROTECT

/*
	D�claration d'une erreur NLib

	@author SOARES Lucas
*/

// ----------------------------
// struct NLib::Erreur::NErreur
// ----------------------------

typedef struct
{
	// D�tail erreur
		// Message
		char *m_message;
		// Fichier
		char *m_fichier;
		// Ligne
		NU32 m_ligne;
		// Code d'erreur
		NCodeErreur m_code;
		NU32 m_codeUtilisateur;

	// Niveau
	NNiveauErreur m_niveau;
} NErreur;

/* Construire l'erreur */
__ALLOC NErreur *NLib_Erreur_NErreur_Construire( NCodeErreur,
	const char *message,
	NU32 codeUtilisateur,
	const char *fichier,
	NU32 ligne,
	NNiveauErreur niveau );

/* D�truire l'erreur */
void NLib_Erreur_NErreur_Detruire( NErreur** );

/* Obtenir codes */
NCodeErreur NLib_Erreur_NErreur_ObtenirCode( const NErreur* );
NU32 NLib_Erreur_NErreur_ObtenirCodeUtilisateur( const NErreur* );

/* Obtenir message */
const char *NLib_Erreur_NErreur_ObtenirMessage( const NErreur* );

/* Obtenir ligne */
NU32 NLib_Erreur_NErreur_ObtenirLigne( const NErreur* );

/* Obtenir fichier */
const char *NLib_Erreur_NErreur_ObtenirFichier( const NErreur* );

/* Obtenir niveau */
NNiveauErreur NLib_Erreur_NErreur_ObtenirNiveau( const NErreur* );

#endif // !NLIB_ERREUR_NERREUR_PROTECT

