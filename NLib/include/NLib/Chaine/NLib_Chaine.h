#ifndef NLIB_CHAINE_PROTECT
#define NLIB_CHAINE_PROTECT

/*
	Gestion des cha�nes de caract�res

	@author SOARES Lucas
*/

// ----------------------
// namespace NLib::Chaine
// ----------------------

// Est EOF?
NBOOL NLib_Chaine_EstEOF( const char *src,
	NU32 curseur );
NBOOL NLib_Chaine_EstEOF2( NU32 curseur,
	NU32 taille );

// Positionner
NBOOL NLib_Chaine_PlacerAuCaractere( const char *src,
	char,
	NU32 *positionActuelle );

NBOOL NLib_Chaine_PlacerALaChaine( const char *src,
	const char*,
	NU32 *positionActuelle );

// Lire
__ALLOC char *NLib_Chaine_LireEntre( const char *src,
	char c1,
	char c2,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

__ALLOC char *NLib_Chaine_LireEntre2( const char *src,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

__ALLOC char *NLib_Chaine_LireJusqua( const char *src,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

NU32 NLib_Chaine_LireNombreNonSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition );

NU32 NLib_Chaine_LireNombreSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition );

// Obtenir prochain caract�re
// Exclus automatiquement les s�parateurs
char NLib_Chaine_ObtenirProchainCaractere( const char *src,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );
// Demande si on exclus ou pas les s�parateurs
char NLib_Chaine_ObtenirProchainCaractere2( const char *src,
	NU32 *positionActuelle,
	NBOOL estConserverPosition,
	NBOOL estInclureSeparateur );

// Est vide [Ne contient aucun caract�re autre que des espaces ou taille==0?]
NBOOL NLib_Chaine_EstVide( const char* );


#endif // !NLIB_CHAINE_PROTECT

