#ifndef NLIB_PREPROCESSEUR_ASSERTION_PROTECT
#define NLIB_PREPROCESSEUR_ASSERTION_PROTECT

/*
	Assertions

	@author SOARES Lucas
*/

// assertion statique
#if defined( WIN32 ) || defined( _WIN64 )
#define nassert( EXP, MSG ) static_assert( EXP, MSG )
#else // WIN32
/* Provient de http://www.pixelbeat.org/programming/gcc/static_assert.html */
#define ASSERT_CONCAT_( a, b ) a##b
#define ASSERT_CONCAT( a, b ) ASSERT_CONCAT_( a, b )
#define nassert( EXP, MSG ) enum { ASSERT_CONCAT( assert_line_, __LINE__ ) = 1 / ( !!( EXP ) ) }
#endif // !WIN32

#endif // !NLIB_PREPROCESSEUR_ASSERTION_PROTECT

