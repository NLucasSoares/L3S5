#ifndef NLIB_PREPROCESSEUR_DEFINITIONS_PROTECT
#define NLIB_PREPROCESSEUR_DEFINITIONS_PROTECT

/*
	D�finitions de pr�processeur

	@author SOARES Lucas
*/

// Will alloc memory
#define __ALLOC

// Output
#define __OUTPUT

// Control will be gained by function once passed
#define __WILLBEOWNED

// Fonction utilis�e en tant que callback
#define __CALLBACK

// Doit �tre lib�r�
#define __MUSTBEFREED

// Windows?
#if defined( WIN32 ) || defined( _WIN64 )
#define IS_WINDOWS
#endif // WIN32 || _WIN64

// D�bogage
#define NLIB_ERREUR_NOTIFICATION_DEBOGUER_LOG

// Pour le module r�seau, transfert s�curis� via buffer avec ent�te+checksum
//#define NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT

// Modules (� d�finir dans la ligne de pr�processeur du projet)
/*
	NLIB_MODULE_RESEAU:
		Active le module r�seau incluant
			- NCachePacket: Gestionnaire de packets sous forme de file
			- NClient: Gestionnaire client avec emission/reception packet
				=> Callbacks pour reception packet/deconnexion du serveur
				=> NCachePacket
			- NServeur: Gestionnaire serveur avec acceptation des clients,
					gestion des emissions/receptions de chaque client
				=> Callbacks pour reception packet/connexion d'un client/
					d�connexion d'un client
				=> NClientServeur: Un client du serveur dans le cache.
				=> NCachePacket pour chaque NClientServeur

	NLIB_MODULE_SDL
		Active le module SDL 2 incluant
			- NFenetre: La fen�tre � manipuler pour cr�er l'application
				graphique
			- NSurface: Une surface standard
			- NTileset: Une grille de surfaces
			- NAnimation: Une grille anim�e de surfaces

	NLIB_MODULE_SDL_IMAGE
		Active le module SDL Image (n�cessitant le module SDL) incluant
			- IMG_Load( )

	NLIB_MODULE_SDL_TTF
		Active le module SDL TTF (n�cessitant le module SDL) incluant
			- NPolice: Donn�es d'une police

	NLIB_MODULE_FMODEX
		Active le module audio FModex

	NLIB_MODULE_REPERTOIRE
		Active le module r�pertoire
*/

#endif // !NLIB_PREPROCESSEUR_DEFINITIONS_PROTECT

