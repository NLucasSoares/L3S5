#ifndef NLIB_PREPROCESSEUR_VERIFICATION_PROTECT
#define NLIB_PREPROCESSEUR_VERIFICATION_PROTECT

/*
	Vérifications pré-processeur

	@author SOARES Lucas
*/

// Vérifier modules
// SDL
#if defined( NLIB_MODULE_SDL_TTF ) || defined( NLIB_MODULE_SDL_IMAGE )
#ifndef NLIB_MODULE_SDL
#error Les modules TTF et IMG nécessitent la SDL.
#endif // !NLIB_MODULE_SDL
#endif // NLIB_MODULE_TTF || NLIB_MODULE_IMAGE

#endif // !NLIB_PREPROCESSEUR_VERIFICATION_PROTECT

