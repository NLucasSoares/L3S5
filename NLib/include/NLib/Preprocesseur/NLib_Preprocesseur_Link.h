#ifndef NLIB_PREPROCESSEUR_LINK_PROTECT
#define NLIB_PREPROCESSEUR_LINK_PROTECT

/*
	Linkage automatique

	@author SOARES Lucas
*/

// Le link via #pragma n'est possible que sous vs
#ifdef IS_WINDOWS

// R�seau
#ifdef NLIB_MODULE_RESEAU
#pragma comment( lib, "Ws2_32.lib" )
#endif // NLIB_MODULE_RESEAU

// SDL2
#ifdef NLIB_MODULE_SDL
#pragma comment( lib, "SDL2.lib" )
#endif // NLIB_MODULE_SDL

// SDL2 image
#ifdef NLIB_MODULE_SDL_IMAGE
#pragma comment( lib, "SDL2_image.lib" )
#endif // NLIB_MODULE_SDL_IMAGE

// SDL2 TTF
#ifdef NLIB_MODULE_SDL_TTF
#pragma comment( lib, "SDL2_ttf.lib" )
#endif // NLIB_MODULE_SDL_TTF

// FModex
#ifdef NLIB_MODULE_FMODEX
#pragma comment( lib, "fmodex_vc.lib" )
#endif // NLIB_MODULE_FMODEX

#endif // IS_WINDOWS

#endif // !NLIB_PREPROCESSEUR_LINK_PROTECT

