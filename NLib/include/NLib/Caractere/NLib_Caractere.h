#ifndef NLIB_CARACTERE_PROTECT
#define NLIB_CARACTERE_PROTECT

/*
	Gestion des caract�res

	@author SOARES Lucas
*/

// -------------------------
// namespace NLib::Caractere
// -------------------------

/* Est un chiffre? */
NBOOL NLib_Caractere_EstUnChiffre( char );
NBOOL NLib_Caractere_EstUnChiffreHexadecimal( char );

/* Est une lettre? */
NBOOL NLib_Caractere_EstUneLettre( char );

/* Est un s�parateur? */
NBOOL NLib_Caractere_EstUnSeparateur( char );

/* Est un caract�re acceptable dans HTML */
NBOOL NLib_Caractere_EstUnCaractereViableHTMLHref( char );

/* Convertir un chiffre en d�cimal */
NU32 NLib_Caractere_ConvertirDecimal( char );

/* Convertir un nombre en caract�re entre 0 et F */
char NLib_Caractere_ConvertirHexadecimal( NU8 nb );

#endif // !NLIB_CARACTERE_PROTECT

