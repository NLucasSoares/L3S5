#ifndef NLIB_FICHIER_NFICHIERTEXTE_PROTECT
#define NLIB_FICHIER_NFICHIERTEXTE_PROTECT

/*
	Fichier texte, contenant des mots lisibles
	et interpr�tables par l'humain

	@author SOARES Lucas
*/

// -----------------------------------
// struct NLib::Fichier::NFichierTexte
// -----------------------------------

typedef struct
{
	// Fichier
	FILE *m_fichier;

	// Est lecture?
	NBOOL m_estLecture;

	// Taille
	NU32 m_taille;
} NFichierTexte;

/* Construire */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu );
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireLecture( const char *lien );

/* D�truire */
void NLib_Fichier_NFichierTexte_Detruire( NFichierTexte** );

/* Est EOF? */
NBOOL NLib_Fichier_NFichierTexte_EstEOF( const NFichierTexte* );

/* Obtenir taille */
NU32 NLib_Fichier_NFichierTexte_ObtenirTaille( const NFichierTexte* );

/* Est lecture? */
NBOOL NLib_Fichier_NFichierTexte_EstLecture( const NFichierTexte* );

/* Est �criture? */
NBOOL NLib_Fichier_NFichierTexte_EstEcriture( const NFichierTexte* );

/* Lire un caract�re */
char NLib_Fichier_NFichierTexte_LireCaractere( NFichierTexte*,
	NBOOL estConservePosition );

/* Lire un nombre */
NU32 NLib_Fichier_NFichierTexte_LireNombre( NFichierTexte*,
	NBOOL estSigne,
	NBOOL estConservePosition );

/* Lire un mot */
__ALLOC char *NLib_Fichier_NFichierTexte_LireMot( NFichierTexte*,
	NBOOL estConservePosition );

/* Lire ligne */
__ALLOC char *NLib_Fichier_NFichierTexte_LireLigne( NFichierTexte*,
	NBOOL estConservePosition );

/* Obtenir ligne sans commentaire */
__ALLOC char *NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( NFichierTexte*,
	char caractereCommentaire );

/* Un mot encadr� de caract�res (si caract�re gauche == '\0', non pris en compte) */
__ALLOC char *NLib_Fichier_NFichierTexte_Lire( NFichierTexte*,
	char caractereGauche,
	char caractereDroit,
	NBOOL estConservePosition );
__ALLOC char *NLib_Fichier_NFichierTexte_Lire2( NFichierTexte*,
	char caractereEncadrement,
	NBOOL estConserverPosition );

/* Ecrire */
NBOOL NLib_Fichier_NFichierTexte_Ecrire( NFichierTexte*,
	NU32 nb );
NBOOL NLib_Fichier_NFichierTexte_Ecrire2( NFichierTexte*,
	NS32 nb );
NBOOL NLib_Fichier_NFichierTexte_Ecrire3( NFichierTexte*,
	const char* );
NBOOL NLib_Fichier_NFichierTexte_Ecrire4( NFichierTexte*,
	float );
NBOOL NLib_Fichier_NFichierTexte_Ecrire5( NFichierTexte*,
	double );

/* Positionner apr�s prochain caract�re */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchainCaractere( NFichierTexte*,
	char );

/* Positionner apr�s prochaine cha�ne */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( NFichierTexte*,
	const char* );

#endif // !NLIB_FICHIER_NFICHIERTEXTE_PROTECT

