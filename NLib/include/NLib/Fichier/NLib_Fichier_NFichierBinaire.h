#ifndef NLIB_FICHIER_NFICHIERBINAIRE_PROTECT
#define NLIB_FICHIER_NFICHIERBINAIRE_PROTECT

// -------------------------------------
// struct NLib::Fichier::NFichierBinaire
// -------------------------------------

typedef struct
{
	// Fichier
	FILE *m_fichier;

	// Est lecture?
	NBOOL m_estLecture;

	// Taille
	NU32 m_taille;
} NFichierBinaire;

/* Construire */
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu );
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireLecture( const char *lien );

/* D�truire */
void NLib_Fichier_NFichierBinaire_Detruire( NFichierBinaire** );

/* Est EOF? */
NBOOL NLib_Fichier_NFichierBinaire_EstEOF( const NFichierBinaire* );

/* Obtenir taille */
NU32 NLib_Fichier_NFichierBinaire_ObtenirTaille( const NFichierBinaire* );

/* Est lecture? */
NBOOL NLib_Fichier_NFichierBinaire_EstLecture( const NFichierBinaire* );

/* Est �criture? */
NBOOL NLib_Fichier_NFichierBinaire_EstEcriture( const NFichierBinaire* );

/* Lire */
__ALLOC char *NLib_Fichier_NFichierBinaire_Lire( NFichierBinaire*,
	NU32 taille );
NBOOL NLib_Fichier_NFichierBinaire_Lire2( NFichierBinaire*,
	__OUTPUT char *sortie,
	NU32 taille );

/* Ecrire */
NBOOL NLib_Fichier_NFichierBinaire_Ecrire( NFichierBinaire*,
	const char *buffer,
	NU32 tailleBuffer );

#endif // !NLIB_FICHIER_NFICHIERBINAIRE_PROTECT

