#ifndef NLIB_TYPE_RECT_PROTECT
#define NLIB_TYPE_RECT_PROTECT

/*
	Définition de rectangles

	@author SOARES Lucas
*/

// --------------------------
// namespace NLib::Type::Rect
// --------------------------

// struct NLib::Type::Rect::NURect
#include "NLib_Type_Rect_NURect.h"

// struct NLib::Type::Rect::NSRect
#include "NLib_Type_Rect_NSRect.h"

#endif // !NLIB_TYPE_RECT_PROTECT

