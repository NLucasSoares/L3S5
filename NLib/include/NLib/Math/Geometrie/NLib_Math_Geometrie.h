#ifndef NLIB_MATH_GEOMETRIE_PROTECT
#define NLIB_MATH_GEOMETRIE_PROTECT

/*
	Ensemble de fonctions géométriques

	@author SOARES Lucas
*/

// -------------------------------
// namespace NLib::Math::Geometrie
// -------------------------------

/* Intersection entre deux zones rectangulaires */
NSRect NLib_Math_Geometrie_CalculerIntersection( NSPoint p1,
	NUPoint t1,
	NSPoint p2,
	NUPoint t2 );

/* Est intersection nulle? */
NBOOL NLib_Math_Geometrie_EstIntersectionNulle( NSRect );

/* Collision entre deux zones rectangulaires? */
NBOOL NLib_Math_Geometrie_EstColision( NSPoint p1,
	NUPoint t1,
	NU32 epaisseurC1,
	NSPoint p2,
	NUPoint t2,
	NU32 epaisseurC2 );

/* Colision entre un rectangle et un point? */
NBOOL NLib_Math_Geometrie_EstColision2( NSPoint p1,
	NUPoint t1,
	NSPoint p2 );

#endif // !NLIB_MATH_GEOMETRIE_PROTECT

