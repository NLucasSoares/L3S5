#ifndef NLIB_MATH_GRAPHE_FICHIER_NSOMMETFICHIERGRAPHE_PROTECT
#define NLIB_MATH_GRAPHE_FICHIER_NSOMMETFICHIERGRAPHE_PROTECT

// --------------------------------------------------------
// struct NLib::Math::Graphe::Fichier::NSommetFichierGraphe
// --------------------------------------------------------

typedef struct NSommetFichierGraphe
{
	// Identifiant
	NU32 m_identifiant;

	// Nombre prédécesseurs
	NU32 m_nombrePredecesseur;

	// Nombre successeurs
	NU32 m_nombreSuccesseur;

	// Prédécesseurs
	NU32 *m_predecesseur;

	// Successeur
	NU32 *m_successeur;
	NS32 *m_valeurSuccesseur;
} NSommetFichierGraphe;

/* Construire */
__ALLOC NSommetFichierGraphe *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Construire( NFichierTexte*,
	NU32 identifiantSommet,
	NBOOL estValue );

/* Détruire */
void NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Detruire( NSommetFichierGraphe** );

/* Obtenir identifiant */
NU32 NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirIdentifiant( const NSommetFichierGraphe* );

/* Obtenir nombre prédécesseurs */
NU32 NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombrePredecesseur( const NSommetFichierGraphe* );

/* Obtenir nombre successeurs */
NU32 NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombreSuccesseur( const NSommetFichierGraphe* );

/* Obtenir prédecesseurs */
const NU32 *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirPredecesseur( const NSommetFichierGraphe* );

/* Obtenir successeurs */
const NU32 *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirSuccesseur( const NSommetFichierGraphe* );
const NS32 *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirValeurSuccesseur( const NSommetFichierGraphe* );

#endif // !NLIB_MATH_GRAPHE_FICHIER_NSOMMEFICHIERGRAPHE_PROTECT

