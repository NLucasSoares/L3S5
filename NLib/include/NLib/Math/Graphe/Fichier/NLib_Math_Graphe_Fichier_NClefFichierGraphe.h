#ifndef NLIB_MATH_GRAPHE_FICHIER_NCLEFFICHIERGRAPHE_PROTECT
#define NLIB_MATH_GRAPHE_FICHIER_NCLEFFICHIERGRAPHE_PROTECT

// ----------------------------------------------------
// enum NLib::Math::Graphe::Fichier::NClefFichierGraphe
// ----------------------------------------------------

typedef enum NClefFichierGraphe
{
	NCLEF_FICHIER_GRAPHE_NOMBRE_SOMMET,
	NCLEF_FICHIER_GRAPHE_EST_VALUE,

	NCLEF_FICHIER_GRAPHE_OUI,
	NCLEF_FICHIER_GRAPHE_NON,

	NCLEF_FICHIER_GRAPHE_SOMMET,

	NCLEF_FICHIER_GRAPHE_SUCCESSEUR,
	NCLEF_FICHIER_GRAPHE_PREDECESSEUR,

	NCLEFS_FICHIER_GRAPHE
} NClefFichierGraphe;

/* Obtenir la clef */
const char *NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NClefFichierGraphe );

#ifdef NLIB_MATH_GRAPHE_FICHIER_NCLEFFICHIERGRAPHE_INTERNE
const char NClefFichierGrapheTexte[ NCLEFS_FICHIER_GRAPHE ][ 32 ] =
{
	"Nombre sommet:",
	"Est value:",

	"OUI",
	"NON",

	"Sommet",

	"suc",
	"pre"
};
#endif // NLIB_MATH_GRAPHE_FICHIER_NCLEFFICHIERGRAPHE_INTERNE

#endif // !NLIB_MATH_GRAPHE_FICHIER_NCLEFFICHIERGRAPHE_PROTECT

