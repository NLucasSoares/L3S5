#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NMATRICECREUSE_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NMATRICECREUSE_PROTECT

// --------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NMatriceCreuse
// --------------------------------------------------------------------

typedef struct NMatriceCreuse
{
	int V;
} NMatriceCreuse;

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NMATRICECREUSE_PROTECT

