#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NMATRICEADJACENCE_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NMATRICEADJACENCE_PROTECT

// -----------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NMatriceAdjacence
// -----------------------------------------------------------------------

typedef struct NMatriceAdjacence
{
	// Nombre sommets
	NU32 m_nombreSommets;

	// Est valu�e?
	NBOOL m_estValue;

	// Matrice adjacence
	NU32 **m_matrice;

	// Matrice valeur
	NS32 **m_valeur;
} NMatriceAdjacence;

/* Construire */
__ALLOC NMatriceAdjacence *NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_Construire( const NFichierGraphe *fichier );

/* D�truire */
void NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_Detruire( NMatriceAdjacence** );

/* Est orient�? */
NBOOL NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_EstOriente( const NMatriceAdjacence* );

/* Est valu�? */
NBOOL NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_EstValue( const NMatriceAdjacence* );

/* Obtenir nombre sommets */
NU32 NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_ObtenirNombreSommet( const NMatriceAdjacence* );

/* Obtenir matrice */
const NU32 **NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_ObtenirMatrice( const NMatriceAdjacence* );

/* Obtenir valeur */
const NS32 **NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_ObtenirValeur( const NMatriceAdjacence* );

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NMATRICEADJACENCE_PROTECT

