#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTEADJACENCE_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTEADJACENCE_PROTECT

// ---------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NListeAdjacence
// ---------------------------------------------------------------------

typedef struct NListeAdjacence
{
	int V;
} NListeAdjacence;

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTEADJACENCE_PROTECT

