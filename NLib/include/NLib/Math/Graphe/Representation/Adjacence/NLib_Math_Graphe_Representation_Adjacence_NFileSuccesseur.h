#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NFILESUCCESSEUR_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NFILESUCCESSEUR_PROTECT

// ---------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NFileSuccesseur
// ---------------------------------------------------------------------

typedef struct NFileSuccesseur
{
	// Nombre sommets
	NU32 m_nombreSommet;

	// Est valu�?
	NBOOL m_estValue;

	// File des Successeurs
	NU32 *m_fileSuccesseur;

	// Adresse du Premier Successeur
	NU32 *m_adressePremierSuccesseur;

	// Valeur ar�tes
	NS32 *m_valeur;
} NFileSuccesseur;

/* Construire */
__ALLOC NFileSuccesseur *NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_Construire( const NFichierGraphe* );

/* D�truire */
void NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_Detruire( NFileSuccesseur** );

/* Obtenir successeurs */
const NU32 *NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_ObtenirSuccesseur( const NFileSuccesseur*,
	NU32 sommet );

/* Obtenir nombre successeurs */
NU32 NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_ObtenirNombreSuccesseur( const NFileSuccesseur*,
	NU32 sommet );

/* Est valu�? */

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NFILESUCCESSEUR_PROTECT

