#ifndef NLIB_MATH_PROBABILITE_NEXPERIENCE_PROTECT
#define NLIB_MATH_PROBABILITE_NEXPERIENCE_PROTECT

// -------------------------------------------
// struct NLib::Math::Probabilite::NExperience
// -------------------------------------------

typedef struct
{
	// Probabilit�s de chaque �venement
	NU32 *m_probabilite;

	// Nombre d'�venements
	NU32 m_nombreEvenement;
} NExperience;

/* Construire (proba entre 0 et 100, avec total <= 100) */
__ALLOC NExperience *NLib_Math_Probabilite_NExperience_Construire( const NU32 *probabilite,
	NU32 nombreProbabilite );

/* D�truire */
void NLib_Math_Probabilite_NExperience_Detruire( NExperience** );

/* Faire une exp�rience (retourne l'identifiant de l'�venement) */
NU32 NLib_Math_Probabilite_NExperience_Lancer( const NExperience* );

#endif // !NLIB_MATH_PROBABILITE_NEXPERIENCE_PROTECT

