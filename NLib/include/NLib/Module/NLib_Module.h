#ifndef NLIB_MODULE_PROTECT
#define NLIB_MODULE_PROTECT

// ----------------------
// namespace NLib::Module
// ----------------------

// namespace NLib::Module::Reseau
#include "Reseau/NLib_Module_Reseau.h"

// namespace NLib::Module::SDL
#include "SDL/NLib_Module_SDL.h"

// namespace NLib::Module::Repertoire
#include "Repertoire/NLib_Module_Repertoire.h"

// namespace NLib::Module::FModex
#include "FModex/NLib_Module_FModex.h"

// enum NLib::Module::NEtapeModule
#include "NLib_Module_NEtapeModule.h"

/* Initialiser les modules */
NBOOL NLib_Module_Initialiser( void );

/* D�truire les modules */
void NLib_Module_Detruire( void );

#endif // !NLIB_MODULE_PROTECT

