#ifndef NLIB_MODULE_REPERTOIRE_NREPERTOIRE_PROTECT
#define NLIB_MODULE_REPERTOIRE_NREPERTOIRE_PROTECT

/*
	R�pertoire contenant des NElementRepertoire

	@author SOARES Lucas
*/

// --------------------------------------------
// struct NLib::Module::Repertoire::NRepertoire
// --------------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE
// struct NLib::Module::Repertoire::NRepertoire
typedef struct
{
	// Le r�pertoire a �t� list�?
	NBOOL m_estListe;

	// Nombre d'�lements
	NU32 m_nbElements;

	// El�ments du repertoire
	NElementRepertoire **m_elements;
} NRepertoire;

/* Construire l'objet */
__ALLOC NRepertoire *NLib_Module_Repertoire_NRepertoire_Construire( void );

/* D�truire l'objet */
void NLib_Module_Repertoire_NRepertoire_Detruire( NRepertoire** );

/* Lister les fichiers */
NBOOL NLib_Module_Repertoire_NRepertoire_Lister( NRepertoire*,
	const char *filtre,
	NAttributRepertoire );

/* Obtenir nombre fichiers */
NU32 NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( const NRepertoire* );

/* Obtenir fichier */
const NElementRepertoire *NLib_Module_Repertoire_NRepertoire_ObtenirFichier( const NRepertoire*,
	NU32 index );

/* Est fichier existe? */
NBOOL NLib_Module_Repertoire_NRepertoire_EstFichierExiste( const NRepertoire*,
	const char *nom );

#endif // NLIB_MODULE_REPERTOIRE

#endif // !NLIB_MODULE_REPERTOIRE_NREPERTOIRE_PROTECT

