#ifndef NLIB_MODULE_FMODEX_NSON_PROTECT
#define NLIB_MODULE_FMODEX_NSON_PROTECT

/*
	Un son g�r� via FModex

	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Module::FModex::NSon
// ---------------------------------

#ifdef NLIB_MODULE_FMODEX
// struct NLib::Module::FModex::NSon
typedef struct
{
	// Son
	FMOD_SOUND *m_son;

	// Volume (Entre 1 et 100)
	NU32 m_volume;

	// Contexte
	const FMOD_SYSTEM *m_contexte;
} NSon;

/* Construire */
__ALLOC NSon *NLib_Module_FModex_NSon_Construire( const char*,
	NU32 volume );

/* D�truire */
void NLib_Module_FModex_NSon_Detruire( NSon** );

/* Lire */
NBOOL NLib_Module_FModex_NSon_Lire( NSon* );

/* Changer le volume */
NBOOL NLib_Module_FModex_NSon_DefinirVolume( NSon*,
	NU32 );

#endif // NLIB_MODULE_FMODEX

#endif // !NLIB_MODULE_FMODEX_NSON_PROTECT

