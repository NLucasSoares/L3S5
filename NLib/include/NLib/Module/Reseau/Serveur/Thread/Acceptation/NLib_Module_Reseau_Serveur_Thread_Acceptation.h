#ifndef NLIB_MODULE_RESEAU_SERVEUR_THREAD_ACCEPTATION_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_THREAD_ACCEPTATION_PROTECT

/*
	Thread d'acceptation des clients

	@author SOARES Lucas
*/

// ------------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Acceptation
// ------------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Thread d'acceptation des clients serveur
void NLib_Module_Reseau_Serveur_Thread_Acceptation_Thread( NServeur* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_THREAD_ACCEPTATION_PROTECT

