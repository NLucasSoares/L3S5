#ifndef NLIB_MODULE_RESEAU_SERVEUR_NSERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_NSERVEUR_PROTECT

/*
	Serveur auto-g�r� pour l'�mission et la reception de
	packets, ainsi que pour la connexion/d�connexion de
	clients.

	Communication avec une gestion ext�rieure via les ca
	llbacks:
		callbackReceptionPacket: Transmet le packet re�u
		ainsi que le client ayant re�u ce packet
		callbackConnexion: Transmet le client qui vient
		de se connecter
		callbackDeconnexion: Transmet le client qui vien
		t de se d�connecter

	@author SOARES Lucas
*/

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------
// struct NLib::Module::Reseau::Serveur::NServeur
// ----------------------------------------------

// Connexion simulatan�es max
#define NMAXIMUM_CLIENTS_SIMULTANES_NSERVEUR	30

// struct NLib::Module::Reseau::Serveur::NServeur
typedef struct
{
	// Socket du serveur
	SOCKET m_socket;

	// Nombre maximum de clients
	NU32 m_nombreMaximumClient;

	// Adressage serveur
	SOCKADDR_IN m_adressageServeur;

	// Clients serveur
	NClientServeur **m_clients;

	// Nombre de clients
	NU32 m_nombreClients;

	// Thread d'acceptation client
	HANDLE m_threadAcceptationClient;

	// Connexion autoris�e?
	NBOOL m_estConnexionAutorisee;

	// Donn�es � transmettre au NClientServeur lors de sa connexion
	void *m_donneePourClient;

	// Derni�re update titre serveur
	NU32 m_tempsDernierUpdateTitre;

	// Mutex
	NMutex *m_mutex;

	// Callbacks
		// Reception packet
			__CALLBACK NBOOL ( *m_callbackReceptionPacket )( const NClientServeur*,
				const NPacket* );
		// Connexion
			__CALLBACK NBOOL ( *m_callbackConnexion )( const NClientServeur* );
		// D�connexion
			__CALLBACK NBOOL ( *m_callbackDeconnexion )( const NClientServeur* );

	// Est en cours
	NBOOL m_estEnCours;

	// Flag sortie
	NBOOL m_flagSortie;

	// Temps avant timeout recv
	NU32 m_tempsAvantTimeout;
} NServeur;

// Construire le serveur
__ALLOC NServeur *NLib_Module_Reseau_Serveur_NServeur_Construire( NU32 port,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NClientServeur*,
		const NPacket* ),
	__CALLBACK NBOOL ( *m_callbackConnexion )( const NClientServeur* ),
	__CALLBACK NBOOL ( *m_callbackDeconnexion )( const NClientServeur* ),
	void *donneePourClient,
	NU32 tempsAvantTimeout );

// D�truire le serveur
void NLib_Module_Reseau_Serveur_NServeur_Detruire( NServeur** );

// Est en cours
NBOOL NLib_Module_Reseau_Serveur_NServeur_EstEnCours( const NServeur* );

// Obtenir le nombre de clients
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreClients( const NServeur* );

// Accepter un client
NBOOL NLib_Module_Reseau_Serveur_NServeur_AccepterClient( NServeur* );

// Nettoyer les clients morts
NU32 NLib_Module_Reseau_Serveur_NServeur_NettoyerClients( NServeur* );

// Tuer tous les clients
void NLib_Module_Reseau_Serveur_NServeur_TuerTousClients( NServeur* );

// Update serveur
void NLib_Module_Reseau_Serveur_NServeur_Update( NServeur* );

// Autoriser la connexion
void NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( NServeur* );

// Interdire la connexion
void NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( NServeur* );

// Obtenir le nombre maximum de clients
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreMaximumClients( const NServeur* );

// D�finir le nombre maximum de clients
void NLib_Module_Reseau_Serveur_NServeur_DefinirNombreMaximumClients( NServeur*,
	NU32 );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_NSERVEUR_PROTECT

