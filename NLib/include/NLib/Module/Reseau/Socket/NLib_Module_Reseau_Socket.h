#ifndef NLIB_MODULE_RESEAU_SOCKET_PROTECT
#define NLIB_MODULE_RESEAU_SOCKET_PROTECT

/*
	Fonction complémentaires pour la manipulation des sockets

	@author SOARES Lucas
*/

// --------------------------------------
// namespace NLib::Module::Reseau::Socket
// --------------------------------------

#ifdef NLIB_MODULE_RESEAU

// namespace NLib::Module::Reseau::Socket::Selection
#include "Selection/NLib_Module_Reseau_Socket_Selection.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SOCKET_PROTECT

