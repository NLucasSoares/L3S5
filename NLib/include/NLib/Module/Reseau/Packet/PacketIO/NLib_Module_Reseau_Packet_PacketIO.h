#ifndef NLIB_MODULE_RESEAU_PACKET_PACKETIO_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PACKETIO_PROTECT

/*
	Gestion de la transmission (recv/send), avec buff�risation
	par tranche de NBUFFER_PACKET (version s�curis�e transfert)

	Inclusion syst�matique de NHEADER_BUFFER et du CRC32 pour 
	les packets CONTENU, et de la taille � envoyer pour les p
	ackets HEADER

	[Packet header]
	"NHDRNLIBNPACKETH"+CRC32(4octets)+TailleDesDonn�es(4octets)+Donn�es(?octets)

	[Packet contenu]
	"NHDRNLIBNPACKETH"+CRC32(4octets)+Donn�es(?octets)

	@author SOARES Lucas
*/

// ------------------------------------------------
// namespace NLib::Module::Reseau::Packet::PacketIO
// ------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
#ifndef NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
// Taille minimale pour le buffer
#define NBUFFER_PACKET_MINIMUM		(NU32)24

// Header buffer
#define NHEADER_BUFFER				"NHDRNLIBNPACKETH"
#define NTAILLE_HEADER_BUFFER		16

// V�rifier la taille
nassert( ( NBUFFER_PACKET >= NBUFFER_PACKET_MINIMUM ),
	"La taille du buffer est incorrecte." );

// enum NLib::Reseau::Packet::PacketIO::NEtapePacketIO
#include "NLib_Module_Reseau_Packet_PacketIO_NEtapePacketIO.h"

// Recevoir un packet
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacket( SOCKET socket,
	NBOOL unused );

// Envoyer un packet
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacket( const NPacket *packet,
	SOCKET socket );
#endif // !NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PACKETIO_PROTECT

