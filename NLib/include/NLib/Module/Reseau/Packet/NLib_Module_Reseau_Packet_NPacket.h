#ifndef NLIB_MODULE_RESEAU_PACKET_NPACKET_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_NPACKET_PROTECT

/*
	D�finition d'un NPacket � transmettre/re�u

	@author SOARES Lucas
*/

// --------------------------------------------
// struct NLib::Module::Reseau::Packet::NPacket
// --------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NPacket
typedef struct
{
	// Taille
	NU32 m_taille;

	// Donn�es
	char *m_data;
} NPacket;

// Construire le packet
__ALLOC NPacket *NLib_Module_Reseau_Packet_NPacket_Construire( void );
__ALLOC NPacket *NLib_Module_Reseau_Packet_NPacket_Construire2( const NPacket *src );
__ALLOC NPacket *NLib_Module_Reseau_Packet_NPacket_Construire3( const char *data,
	NU32 taille );

// D�truire le packet
void NLib_Module_Reseau_Packet_NPacket_Detruire( NPacket** );

// Obtenir la taille
NU32 NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( const NPacket* );

// Obtenir les donn�es
const char *NLib_Module_Reseau_Packet_NPacket_ObtenirData( const NPacket* );

// Ajout donn�es
NBOOL NLib_Module_Reseau_Packet_NPacket_AjouterData( NPacket*,
	const char*,
	NU32 );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_NPACKET_PROTECT

