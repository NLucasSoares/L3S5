#ifndef NLIB_MODULE_SDL_SAISIE_NSAISIESDL_PROTECT
#define NLIB_MODULE_SDL_SAISIE_NSAISIESDL_PROTECT

/*
	Objet permettant une saisie contextuelle dans un objet
	de type NFenetre

	Prend 3 callback � la cr�ation:
		callbackGestion: Sera appel� � chaque tour dans la
			saisie, afin de mettre � jour ce qui doit l'�t
			re durant la p�riode de saisie
		callbackActualisation: Se appel� afin de mettre �
		jour la fen�tre conform�ment aux attentes utilisat
		eur
		callbackUpdateChaine: Sera appel� d�s que la cha�n
		e sera modifi�e par l'utilisateur

	Il est possible de personnaliser la m�thode de sortie
	via une gestion de NBouton � cliquer/un changement de
	la touche de validation

	Il est aussi possible d'effectuer une gestion externe
	de la saisie en passant un pointeur sur l'�tat "en cou
	rs"

	@author SOARES Lucas
*/

#ifdef NLIB_MODULE_SDL

// ---------------------------------------------
// struct NLib::Modules::SDL::Saisie::NSaisieSDL
// ---------------------------------------------

typedef struct
{
	// Callbacks
		// Fonctions
			__CALLBACK void ( *m_callbackGestion )( void* );
			__CALLBACK void ( *m_callbackActualisation )( void* );
			__CALLBACK NBOOL ( *m_callbackUpdateChaine )( void* );
		// Arguments
			void *m_argumentCallbackGestion;
			void *m_argumentCallbackActualisation;
			void *m_argumentCallbackUpdateChaine;

	// Mode
	NU32 m_mode;

	// Taille maximale
	NU32 m_tailleMaximale;

	// Cadre limite
	NCadre **m_cadre;
	NU32 m_nbCadres;
	
	// Cadre cliqu�
	NU32 m_idCadreClique;

	// Curseur
	NU32 m_curseur;

	// Touche arr�t
	SDL_Keycode m_touche; 

	// Cha�ne
	char *m_texte;

	// En cours?
	NBOOL *m_estEnCours;
} NSaisieSDL;
					
/* Construire l'objet */
__ALLOC NSaisieSDL *NLib_Module_SDL_Saisie_NSaisieSDL_Construire( NModeSaisieSDL mode,
	NU32 tailleMax,
	SDL_Keycode toucheArret,
	__CALLBACK void ( __cdecl *callbackGestion )( void* ),
	__CALLBACK void ( __cdecl *callbackActualisation )( void* ),
	__CALLBACK NBOOL ( __cdecl *callbackUpdateChaine )( void* ),
	void *argumentCallbackGestion,
	void *argumentCallbackActualisation,
	void *argumentCallbackUpdateChaine,
	const char *chaineInitiale /* = NULL */ );

/* D�truire l'objet */
void NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( NSaisieSDL** );

/* Saisir */
NRetourSaisieSDL NLib_Module_SDL_Saisie_NSaisieSDL_Saisir( NSaisieSDL*,
	SDL_Event *e, // Evenement
	NBOOL *ptrContinuer /* = NULL */, // Handle �tat boucle si externe
	NBOOL autoriseTab /* = NTRUE */, // Autoriser "tabulation"?
	char **callbackChaine /* = NULL */ ); // Stocke en temps r�el le texte saisi

/* D�finir cadres */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirCadres( NSaisieSDL*,
	const NCadre **cadres,
	NU32 nb );

/* D�finir touche arr�t */
void NLib_Module_SDL_Saisie_NSaisieSDL_DefinirToucheArret( NSaisieSDL*,
	SDL_Keycode );

/* D�finir curseur */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirCurseur( NSaisieSDL*,
	NU32 );

/* D�finir texte */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirTexte( NSaisieSDL*,
	const char* );

/* Obtenir texte */
const char *NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( const NSaisieSDL* );

/* Obtenir cadre cliqu� */
NU32 NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirCadreClique( const NSaisieSDL* );

/* Est en cours? */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_EstEnCours( const NSaisieSDL* );

#ifdef NLIB_MODULE_SDL_SAISIE_NSAISIESDL_INTERNE
#define NSAISIE_CARACTERE( a, A, majuscule, caractere ) \
	( caractere = majuscule ? ( A == '\0' ? caractere : A ) : ( a == '\0' ? caractere : a ) )

#define NSAISIE_CARACTERE_VERIFICATION( a, A, majuscule, caractere, CONDITION ) \
	{ \
		if( CONDITION ) \
			NSAISIE_CARACTERE( a, A, majuscule, caractere ); \
	}

#define NSAISIE_CARACTERE_AVEC_ACCENT( a, caractere, accent ) \
	{ \
		caractere = a; \
		accent = NACCENT_SAISIE_SDL_AUCUN; \
	}
#endif // NLIB_MODULE_SDL_SAISIE_NSAISIESDL_INTERNE

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SAISIE_NSAISIESDL_PROTECT

