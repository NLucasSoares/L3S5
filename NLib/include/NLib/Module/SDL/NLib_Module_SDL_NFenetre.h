#ifndef NLIB_MODULE_SDL_NFENETRE_PROTECT
#define NLIB_MODULE_SDL_NFENETRE_PROTECT

/*
	Objet NFenetre fournissant un support
	pour l'affichage de NSurface/NTileset
	/NAnimation/NBouton/NCadre...

	@author SOARES Lucas
*/

// ----------------------------------
// struct NLib::Module::SDL::NFenetre
// ----------------------------------

#ifdef NLIB_MODULE_SDL
// Notre renderer pr�f�r�
#define NLIB_MODULE_SDL_RENDERER		"direct3d"

// struct NLib::Module::SDL::NFenetre
typedef struct
{
	// Fen�tre
	SDL_Window *m_fenetre;

	// Renderer
	SDL_Renderer *m_renderer;

	// R�solution
	NUPoint m_resolution;
	
	// Ev�nement dans la fen�tre
	SDL_Event m_evenement;
} NFenetre;

/* Construire la fen�tre */
__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire( const char *titre,
	NUPoint resolution );
__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire2( const char *titre,
	NUPoint resolution,
	NBOOL estVisible );

/* D�truire la fen�tre */
void NLib_Module_SDL_NFenetre_Detruire( NFenetre** );

/* Obtenir la fen�tre */
SDL_Window *NLib_Module_SDL_NFenetre_ObtenirFenetre( const NFenetre* );

/* Obtenir le renderer */
SDL_Renderer *NLib_Module_SDL_NFenetre_ObtenirRenderer( const NFenetre* );

/* Obtenir la r�solution */
const NUPoint *NLib_Module_SDL_NFenetre_ObtenirResolution( const NFenetre* );

/* Obtenir l'�venement */
SDL_Event *NLib_Module_SDL_NFenetre_ObtenirEvenement( const NFenetre* );

/* Nettoyer la fen�tre */
void NLib_Module_SDL_NFenetre_Nettoyer( NFenetre* );
void NLib_Module_SDL_NFenetre_Nettoyer2( NFenetre*,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a );

/* Dessiner un point */
void NLib_Module_SDL_NFenetre_DessinerPoint( NFenetre*,
	NSPoint,
	NCouleur );

/* Dessiner des points */
void NLib_Module_SDL_NFenetre_DessinerPoints( NFenetre*,
	NSPoint*,
	NU32 nombrePoint,
	NCouleur );

/* Dessiner une ligne */
void NLib_Module_SDL_NFenetre_DessinerLigne( NFenetre*,
	NSPoint,
	NSPoint,
	NCouleur );

/* Dessiner un rectangle */
void NLib_Module_SDL_NFenetre_DessinerRectangle( NFenetre*,
	NSRect,
	NU32 epaisseur,
	NCouleur couleurContour,
	NCouleur couleurFond );
void NLib_Module_SDL_NFenetre_DessinerRectangle2( NFenetre*,
	const SDL_Rect m_coordonnees[ NDIRECTIONS + 1 ],
	const NCouleur *couleurContour,
	const NCouleur *couleurFond );

/* Actualiser le renderer */
void NLib_Module_SDL_NFenetre_Update( NFenetre* );

/* D�finir un clip rect */
void NLib_Module_SDL_NFenetre_DefinirClipRect( NFenetre*,
	NSRect );

/* Supprimer le clip rect */
void NLib_Module_SDL_NFenetre_SupprimerClipRect( NFenetre* );

/* Afficher fen�tre (SDL_WINDOW_SHOWN) */
void NLib_Module_SDL_NFenetre_AfficherFenetre( NFenetre* );

/* Cache fen�tre (SDL_WINDOW_HIDDEN) */
void NLib_Module_SDL_NFenetre_CacheFenetre( NFenetre* );

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_NFENETRE_PROTECT

