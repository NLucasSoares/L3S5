#ifndef NLIB_MODULE_SDL_TTF_PROTECT
#define NLIB_MODULE_SDL_TTF_PROTECT

/*
	Module SDL_TTF

	@author SOARES Lucas
*/

// --------------------------------
// namespace NLib::Module::SDL::TTF
// --------------------------------

#ifdef NLIB_MODULE_SDL_TTF
// SDL_TTF header
#include <SDL2/SDL_ttf.h>

// enum NLib::Module::SDL::TTF::NStylePolice
#include "NLib_Module_SDL_TTF_NStylePolice.h"

// struct NLib::Module::SDL::TTF::NPolice
#include "NLib_Module_SDL_TTF_NPolice.h"

/* Initialiser SDL TTF */
NBOOL NLib_Module_SDL_TTF_Initialiser( void );

/* Detruire la SDL TTF */
void NLib_Module_SDL_TTF_Detruire( void );

#endif // NLIB_MODULE_SDL_TTF

#endif // !NLIB_MODULE_SDL_TTF_PROTECT

