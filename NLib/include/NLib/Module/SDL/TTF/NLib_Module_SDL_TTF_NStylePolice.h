#ifndef NLIB_MODULE_SDL_TTF_NSTYLEPOLICE_PROTECT
#define NLIB_MODULE_SDL_TTF_NSTYLEPOLICE_PROTECT

/*
	Les diff�rents styles de police SDL_TTF
	renomm� dans une �num�ration

	@author SOARES Lucas
*/

// -----------------------------------------
// enum NLib::Module::SDL::TTF::NStylePolice
// -----------------------------------------

#ifdef NLIB_MODULE_SDL_TTF
typedef enum
{
	NSTYLE_POLICE_NORMAL = TTF_STYLE_NORMAL,
	NSTYLE_POLICE_GRAS = TTF_STYLE_BOLD,
	NSTYLE_POLICE_ITALIC = TTF_STYLE_ITALIC,
	NSTYLE_POLICE_SOULIGNE = TTF_STYLE_UNDERLINE,
	NSTYLE_POLICE_BARRE = TTF_STYLE_STRIKETHROUGH
} NStylePolice;

#endif // NLIB_MODULE_SDL_TTF

#endif // !NLIB_MODULE_SDL_TTF_NSTYLEPOLICE_PROTECT

