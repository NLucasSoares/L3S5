#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -----------------------------------
// struct NLib::Fichier::NFichierTexte
// -----------------------------------

/* Construire (priv�e) */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireInterne( const char *lien,
	const char *mode )
{
	// Sortie
	__OUTPUT NFichierTexte *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NFichierTexte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le fichier
	if( !( out->m_fichier = fopen( lien,
		mode ) ) )
	{
		// Notifier
		//NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// D�terminer si il s'agit de lecture/�criture
	switch( mode[ 0 ] )
	{
		case 'w':
			out->m_estLecture = NFALSE;
			break;

		case 'r':
			out->m_estLecture = NTRUE;
			break;

		default:
			break;
	}

	// D�terminer la taille
	if( out->m_estLecture )
		out->m_taille = NLib_Fichier_Operation_ObtenirTaille( out->m_fichier );

	// OK
	return out;
}

/* Construire */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu )
{
	// Mode
	char mode[ 48 ];
	
	// Cr�er mode
	strcpy( mode,
		estEcraserContenu ?
			"w+"
			: "w" );

	// Construire
	return NLib_Fichier_NFichierTexte_ConstruireInterne( lien,
		mode );
}

__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireLecture( const char *lien )
{
	return NLib_Fichier_NFichierTexte_ConstruireInterne( lien,
		"r" );
}

/* D�truire */
void NLib_Fichier_NFichierTexte_Detruire( NFichierTexte **this )
{
	// Fermer le fichier
	fclose( (*this)->m_fichier );

	// Lib�rer
	NFREE( *this );
}

/* Est EOF? */
NBOOL NLib_Fichier_NFichierTexte_EstEOF( const NFichierTexte *this )
{
	return NLib_Fichier_Operation_EstEOF2( this->m_fichier,
		this->m_taille );
}

/* Obtenir taille */
NU32 NLib_Fichier_NFichierTexte_ObtenirTaille( const NFichierTexte *this )
{
	return this->m_estLecture ?
		this->m_taille
		: NFICHIER_CODE_ERREUR;
}

/* Est lecture? */
NBOOL NLib_Fichier_NFichierTexte_EstLecture( const NFichierTexte *this )
{
	return this->m_estLecture;
}

/* Est �criture? */
NBOOL NLib_Fichier_NFichierTexte_EstEcriture( const NFichierTexte *this )
{
	return !this->m_estLecture;
}

/* Lire un caract�re */
char NLib_Fichier_NFichierTexte_LireCaractere( NFichierTexte *this,
	NBOOL estConservePosition )
{
	// Position initiale
	NU32 positionInitiale;

	// Sortie
	__OUTPUT char sortie;

	// V�rifier
	if( NLib_Fichier_NFichierTexte_EstEOF( this )
		|| !this->m_estLecture )
		return '\0';

	// Enregistrer position initiale
	positionInitiale = ftell( this->m_fichier );

	// Lire
	sortie = (char)fgetc( this->m_fichier );

	// Restaurer si n�cessaire
	if( estConservePosition )
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/* Lire un nombre */
NU32 NLib_Fichier_NFichierTexte_LireNombre( NFichierTexte *this,
	NBOOL estSigne,
	NBOOL estConservePosition )
{
	return estSigne ? 
		NLib_Fichier_Operation_LireNombreSigne( this->m_fichier,
			estConservePosition )
		: NLib_Fichier_Operation_LireNombreNonSigne( this->m_fichier,
			estConservePosition );
}

/* Lire un mot */
__ALLOC char *NLib_Fichier_NFichierTexte_LireMot( NFichierTexte *this,
	NBOOL estConservePosition )
{
	// Position initiale
	NU32 positionInitiale;

	// Sortie
	__OUTPUT char *sortie;
	
	// Enregistrer position initiale
	positionInitiale = ftell( this->m_fichier );
	
	// Lire
	sortie = NLib_Fichier_Operation_LireEntre( this->m_fichier,
		'\0',
		'\0' );

	// Restaurer si n�cessaire
	if( estConservePosition )
		// Restaurer
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/* Lire ligne */
__ALLOC char *NLib_Fichier_NFichierTexte_LireLigne( NFichierTexte *this,
	NBOOL estConservePosition )
{
	// Position initiale
	NU32 positionInitiale;

	// Sortie
	__OUTPUT char *sortie;
	
	// Enregistrer position initiale
	positionInitiale = ftell( this->m_fichier );
	
	// Lire
	sortie = NLib_Fichier_Operation_LireEntre( this->m_fichier,
		'\0',
		'\n' );

	// Restaurer si n�cessaire
	if( estConservePosition )
		// Restaurer
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/* Lire prochaine ligne sans commentaire (priv�e) */
__ALLOC char *NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( NFichierTexte *fichier,
	char caractereCommentaire )
{
	// Sortie
	__OUTPUT char *sortie = NULL;

	// Curseur
	NU32 curseur = 0;

	do
	{
		// Lib�rer
		NFREE( sortie );

		// Z�ro
		curseur = 0;

		// Lire
		if( !( sortie = NLib_Fichier_NFichierTexte_LireLigne( fichier,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Quitter
			return NULL;
		}
	} while( NLib_Chaine_ObtenirProchainCaractere2( sortie,
			&curseur,
			NFALSE,
			NFALSE ) == caractereCommentaire
		|| NLib_Chaine_EstVide( sortie ) );

	// OK
	return sortie;
}

/* Lire un mot encadr� de caract�res (si caract�re gauche == '\0', non pris en compte) */
__ALLOC char *NLib_Fichier_NFichierTexte_Lire( NFichierTexte *this,
	char caractereGauche,
	char caractereDroit,
	NBOOL estConserverPosition )
{
	// Sortie
	__OUTPUT char *sortie;
	
	// Position initiale
	NU32 positionInitiale;

	// Enregistrer position initiale
	positionInitiale = ftell( this->m_fichier );

	// Lire
	sortie = NLib_Fichier_Operation_LireEntre( this->m_fichier,
		caractereGauche,
		caractereDroit );

	// Restaurer la position
	if( estConserverPosition )
		// Restaurer
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

__ALLOC char *NLib_Fichier_NFichierTexte_Lire2( NFichierTexte *this,
	char caractereEncadrement,
	NBOOL estConserverPosition )
{
	return NLib_Fichier_NFichierTexte_Lire( this,
		caractereEncadrement,
		caractereEncadrement,
		estConserverPosition );
}

/* Ecrire */
NBOOL NLib_Fichier_NFichierTexte_Ecrire( NFichierTexte *this,
	NU32 nb )
{
	return ( fprintf( this->m_fichier,
		"%u",
		nb ) > 0 );
}

NBOOL NLib_Fichier_NFichierTexte_Ecrire2( NFichierTexte *this,
	NS32 nb )
{
	return ( fprintf( this->m_fichier,
		"%d",
		nb ) > 0 );
}

NBOOL NLib_Fichier_NFichierTexte_Ecrire3( NFichierTexte *this,
	const char *s )
{
	return ( fputs( s,
		this->m_fichier ) >= 0 );
}

NBOOL NLib_Fichier_NFichierTexte_Ecrire4( NFichierTexte *this,
	float nb )
{
	return ( fprintf( this->m_fichier,
		"%f",
		(double)nb ) > 0 );
}

NBOOL NLib_Fichier_NFichierTexte_Ecrire5( NFichierTexte *this,
	double nb )
{
	return ( fprintf( this->m_fichier,
		"%f",
		nb ) > 0 );
}

/* Positionner apr�s prochain caract�re */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchainCaractere( NFichierTexte *this,
	char caractere )
{
	return NLib_Fichier_Operation_PlacerCaractere( this->m_fichier,
		caractere );
}

/* Positionner apr�s prochaine cha�ne */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( NFichierTexte *this,
	const char *chaine )
{
	return NLib_Fichier_Operation_PlacerChaine( this->m_fichier,
		chaine );
}

