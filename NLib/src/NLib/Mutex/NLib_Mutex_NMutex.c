#include "../../../include/NLib/NLib.h"

/* Construire */
__ALLOC NMutex *NLib_Mutex_NMutex_Construire( void )
{
	// Sortie
	__OUTPUT NMutex *out;

	// Allouer
	if( !( out = calloc( 1,
		sizeof( NMutex ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire mutex
	if( !( out->m_mutex = CreateMutex( NULL,
		NFALSE,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/* D�truire */
void NLib_Mutex_NMutex_Detruire( NMutex **this )
{
	// Fermer
#ifdef IS_WINDOWS
	CloseHandle( (*this)->m_mutex );
#else // IS_WINDOWS

#endif // !IS_WINDOWS

	NFREE( *this );
}

/* Lock */
NBOOL NLib_Mutex_NMutex_Lock( NMutex *this )
{
	// Lock
#ifdef IS_WINDOWS
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Quitter
		return NFALSE;
	}
#else // IS_WINDOWS

#endif // !IS_WINDOWS

	// OK
	return NTRUE;
}

/* Unlock */
void NLib_Mutex_NMutex_Unlock( NMutex *this )
{
#ifdef IS_WINDOWS
	ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS

#endif // !IS_WINDOWS
}

