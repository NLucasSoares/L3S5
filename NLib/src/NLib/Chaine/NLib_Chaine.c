#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------
// namespace NLib::Chaine
// ----------------------

/* Est EOF? */
NBOOL NLib_Chaine_EstEOF( const char *src,
	NU32 curseur )
{
	return ( curseur >= strlen( src ) );
}

NBOOL NLib_Chaine_EstEOF2( NU32 curseur,
	NU32 taille )
{
	return ( curseur >= taille );
}

/* Placer le curseur */
NBOOL NLib_Chaine_PlacerAuCaractere( const char *src,
	char c,
	NU32 *positionActuelle )
{
	// Caract�re lu
	char cL;

	// Chercher le caract�re
	do
	{
		// Lire
		cL = src[ *positionActuelle ];

		// Avancer le curseur
		(*positionActuelle)++;
	} while( !NLib_Chaine_EstEOF( src,
		*positionActuelle )
		&& cL != c );

	// OK
	return ( c == cL );
}

NBOOL NLib_Chaine_PlacerALaChaine( const char *src,
	const char *s,
	NU32 *positionActuelle )
{
	// Curseur de recherche
	NU32 curseurRecherche = 0;

	// Longueur de la chaine
	NU32 longueur;

	// Obtenir la longueur
	longueur = strlen( s );

	// Chercher la cha�ne
	while( curseurRecherche < longueur
		&& !NLib_Chaine_EstEOF( src,
			*positionActuelle ) )
		// Impossible de trouver le caract�re
		if( NLib_Chaine_ObtenirProchainCaractere2( src,
			positionActuelle,
			NFALSE,
			NTRUE ) != s[ curseurRecherche ] )
			curseurRecherche = 0;
		// Caract�re en cours trouv�
		else
			curseurRecherche++;

	// OK?
	return ( curseurRecherche >= longueur );
}

/* Lire */
__ALLOC char *NLib_Chaine_LireEntre( const char *src,
	char c1,
	char c2,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Position initiale
	NU32 positionInitiale = *positionActuelle;

	// Buffer
	char *temp;

	// Caract�re lu
	char cL;

	// Curseur pour copie
	NU32 curseur = 0;

	// Caract�re d'�chappement?
	if( c1 != '\0' )
		// Chercher le premier caract�re
		if( !NLib_Chaine_PlacerAuCaractere( src,
			c1,
			positionActuelle ) )
			return NULL;

	// Lire
	do
	{
		// Si le caract�re n'est pas le caract�re de fin
		if( src[ *positionActuelle ] != c2 )
		{
			// La m�moire n'est pas encore allou�e?
			if( out == NULL )
			{
				// Allouer la m�moire
				if( !( out = calloc( 1 /* + 1 caract�re */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Quitter
					return NULL;
				}

				// O� copier?
				curseur = 0;
			}
			else
			{
				// Enregistrer adresse
				temp = out;

				// Allouer le nouveau espace m�moire
				if( !( out = calloc( strlen( temp ) + 1 /* + 1 caract�re */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					NFREE( temp );

					// Quitter
					return NULL;
				}

				// Copier anciennes donn�es
				memcpy( out,
					temp,
					strlen( temp ) );

				// O� placer le prochain caract�re?
				curseur = strlen( temp );

				// Lib�rer buffer
				NFREE( temp );
			}

			// Copier nouveau caract�re
			out[ curseur ] = src[ *positionActuelle ];
		}

		// Lire caract�re
		cL = src[ *positionActuelle ];

		// Incr�menter curseur
		if( !NLib_Chaine_EstEOF( src,
			*positionActuelle ) )
			(*positionActuelle)++;
	} while( !NLib_Chaine_EstEOF( src,
			*positionActuelle )
		&& cL != c2 );

	// Restaurer la position initiale si demand�
	if( estConserverPosition )
		*positionActuelle = positionInitiale;

	// OK
	return out;
}

__ALLOC char *NLib_Chaine_LireEntre2( const char *src,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireEntre( src,
		c,
		c,
		positionActuelle,
		estConserverPosition );
}

__ALLOC char *NLib_Chaine_LireJusqua( const char *src,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireEntre( src,
		'\0',
		c,
		positionActuelle,
		estConserverPosition );
}

/* Lire prochain caract�re */
char NLib_Chaine_ObtenirProchainCaractere2( const char *src,
	NU32 *positionActuelle,
	NBOOL estConserverPosition,
	NBOOL estInclureSeparateur )
{
	// Caract�re lu
	__OUTPUT char cL;

	if( estInclureSeparateur )
	{
		// Lire caract�re
		cL = src[ *positionActuelle ];

		// V�rifier
		

		// Restaurer la position si n�cessaire
		if( !estConserverPosition )
			if( cL != '\0' )
				// Incr�menter curseur
				(*positionActuelle)++;

		// OK
		return cL;
	}
	else
		return NLib_Chaine_ObtenirProchainCaractere( src,
			positionActuelle,
			estConserverPosition );
}

char NLib_Chaine_ObtenirProchainCaractere( const char *src,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	// Caract�re lu
	__OUTPUT char cL;

	// Position initiale
	NU32 positionInitiale = *positionActuelle;

	// Continuer?
	NBOOL estContinuer = NTRUE;

	// Chercher des caract�res non s�parateurs
	do
	{
		// Lire caract�re
		cL = src[ *positionActuelle ];

		// Analyser
		switch( cL )
		{
			case ' ':
			case '\n':
			case '\t':
			case '\r':
			case '\0':
				break;

			default:
				estContinuer = NFALSE;
				break;
		}

		// Incr�menter curseur
		(*positionActuelle)++;
	} while( estContinuer );

	// Restaurer la position si n�cessaire
	if( estConserverPosition )
		*positionActuelle = positionInitiale;

	// OK
	return cL;
}

NU32 NLib_Chaine_LireNombreInterne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition,
	NBOOL estSigne )
{
	// Position initiale
	NU32 positionInitiale;

	// Taille du fichier
	NU32 taille;

	// Caract�re lu
	char caractere;

	// Curseur
	NU32 curseurBuffer = 0;

#define TAILLE_BUFFER		4096

	// Buffer
	char buffer[ TAILLE_BUFFER ] = { 0, };

	// Obtenir la taille
	taille = strlen( chaine );

	// Enregistrer position initiale
	positionInitiale = *curseur;

	// Chercher chiffre ou '-' si sign�
	do
	{
		// Lire
		caractere = chaine[ *curseur ];

		// Avancer
		(*curseur)++;
	} while( ( !NLib_Caractere_EstUnChiffre( caractere )
			&& ( estSigne ? ( caractere != '-' ) : NTRUE ) )
		&& !NLib_Chaine_EstEOF2( *curseur,
			taille ) );

	// Quitter en traduisant
	if( NLib_Chaine_EstEOF2( *curseur,
		taille ) )
	{
		// Restaurer si n�cessaire
		if( estConserverPosition )
			*curseur = positionInitiale;

		// V�rifier
		if( NLib_Caractere_EstUnChiffre( caractere ) )
			// Il s'agit d'un nombre
			return NLib_Caractere_ConvertirDecimal( caractere );
		else
			// Ce n'est pas un nombre
			return NERREUR;
	}

	// Reculer d'une case
	if( *curseur > 0 )
		(*curseur)--;

	// Vider le buffer
	memset( buffer,
		0,
		TAILLE_BUFFER );

	// Lire nombre
	do
	{
		// Lire
		caractere = chaine[ *curseur ];

		// Avancer
		(*curseur)++;

		// V�rifier
		if( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ? ( caractere == '-' && !curseurBuffer ) : NFALSE ) )
			// Ajouter au buffer
			buffer[ curseurBuffer++ ] = caractere;
	} while( ( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ? ( caractere == '-' && curseurBuffer == 1 ) : NFALSE ) )
		&& strlen( buffer ) < TAILLE_BUFFER
		&& !NLib_Chaine_EstEOF2( *curseur,
			taille ) );

#undef TAILLE_BUFFER

	// Restaurer si n�cessaire
	if( estConserverPosition )
		*curseur = positionInitiale;

	// OK
	return strtol( buffer,
		NULL,
		10 );
}

NU32 NLib_Chaine_LireNombreNonSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireNombreInterne( chaine,
		curseur,
		estConserverPosition,
		NFALSE );
}

NU32 NLib_Chaine_LireNombreSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireNombreInterne( chaine,
		curseur,
		estConserverPosition,
		NTRUE );
}

// Est vide [Ne contient aucun caract�re autre que des espaces ou taille==0?]
NBOOL NLib_Chaine_EstVide( const char *src )
{
	// V�rifier taille
	if( !strlen( src ) )
		return NTRUE;

	// V�rifier contenu
	while( *src == ' ' )
		src++;

	// La chaine est vide
	return *src == '\0';
}

