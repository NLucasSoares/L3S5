#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ---------------------
// namespace NLib::Temps
// ---------------------

/* Obtenir le nombre de ms depuis le lancement */
NU32 NLib_Temps_ObtenirTick( void )
{
	return GetTickCount( );
}

NU64 NLib_Temps_ObtenirTick64( void )
{
	return GetTickCount64( );
}

/* Attendre (ms) */
void NLib_Temps_Attendre( NU32 t )
{
#ifdef IS_WINDOWS
	Sleep( t );
#else // IS_WINDOWS
#error METTRE UNE FONCTION SLEEP
#endif // !IS_WINDOWS
}

/* Obtenir nombre al�atoire */
NU32 NLib_Temps_ObtenirNombreAleatoire( void )
{
#ifdef IS_WINDOWS
	// Sortie
	__OUTPUT NU32 sortie;

	// Obtenir
	rand_s( &sortie );

	// OK
	return sortie;
#else // IS_WINDOWS
	return rand( );
#endif // !IS_WINDOWS
}
