#define NLIB_ERREUR_NOTIFICATION_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------
// namespace NLib::Erreur::Notification
// ------------------------------------

/* Notifier */
void NLib_Erreur_Notification_Notifier( __WILLBEOWNED NErreur *erreur )
{
	// Envoyer au callback concern�
	if( erreur )
		m_fluxActif( erreur );
	// Erreur NULL
	else
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

	// D�truire erreur
	NLib_Erreur_NErreur_Detruire( &erreur );
}

/* Flux standard */
void NLib_Erreur_Notification_FluxStandard( const NErreur *erreur )
{
	// Afficher message?
	NBOOL estAfficher = NTRUE;

	// On n'affichera pas les avertissements
#ifndef NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS
	if( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) <= NNIVEAU_ERREUR_AVERTISSEMENT )
		return;
#endif // NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS

	// Doit afficher?
	switch( NLib_Erreur_NErreur_ObtenirCode( erreur ) )
	{
		case NERREUR_SOCKET_RECV:
		case NERREUR_SOCKET_SEND:
			estAfficher = ( NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) != 0 ); // (<=> errno != 0?)
			break;

		default:
			break;
	}

	// Afficher l'erreur
	if( estAfficher )
	{
		// Erreur
		printf( "[%s]: %s( )::(%s)\n",
			NLib_Erreur_NNiveauErreur_Traduire( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) ),
			NLib_Erreur_NErreur_ObtenirMessage( erreur ),
			NLib_Erreur_NCodeErreur_Traduire( NLib_Erreur_NErreur_ObtenirCode( erreur ) ) );
	
		// Origine
		printf( "\"%s\", ligne %d, code %d.\n\n",
			NLib_Erreur_NErreur_ObtenirFichier( erreur ),
			NLib_Erreur_NErreur_ObtenirLigne( erreur ),
			NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) );
	}
}

/* Modifier le flux d'erreur */
NBOOL NLib_Erreur_Notification_ModifierCallbackFluxErreur( __CALLBACK void ( __cdecl *callback )( const NErreur* ) )
{
	// V�rifier
		// Param�tre
			if( !callback )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Enregistrer le flux
	m_fluxActif = callback;

	// OK
	return NTRUE;
}

/* Restaurer flux d'erreur */
void NLib_Erreur_Notification_RestaurerCallbackFluxErreurInitial( void )
{
	m_fluxActif = NLib_Erreur_Notification_FluxStandard;
}

