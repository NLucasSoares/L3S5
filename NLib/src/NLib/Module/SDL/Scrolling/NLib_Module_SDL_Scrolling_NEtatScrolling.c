#include "../../../../../include/NLib/NLib.h"

// ---------------------------------------------------
// struct NLib::Module::SDL::Scrolling::NEtatScrolling
// ---------------------------------------------------

#ifdef NLIB_MODULE_SDL

/* Construire */
__ALLOC NEtatScrolling *NLib_Module_SDL_Scrolling_NEtatScrolling_Construire( void )
{
	// Sortie
	__OUTPUT NEtatScrolling *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NEtatScrolling ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/* D�truire */
void NLib_Module_SDL_Scrolling_NEtatScrolling_Detruire( NEtatScrolling **this )
{
	// Lib�rer
	NFREE( *this );
}

/* Obtenir scrolling */
const NSPoint *NLib_Module_SDL_Scrolling_NEtatScrolling_ObtenirScrolling( const NEtatScrolling *this )
{
	return &this->m_scrolling;
}

/* D�but scrolling */
void NLib_Module_SDL_Scrolling_NEtatScrolling_DebutScrolling( NEtatScrolling *this,
	const NSPoint *souris )
{
	// D�but
	this->m_positionClic = *souris;
	this->m_scrollingInitial = this->m_scrolling;
	this->m_estEnCours = NTRUE;
}

/* Update scrolling */
NBOOL NLib_Module_SDL_Scrolling_NEtatScrolling_UpdateScrolling( NEtatScrolling *this,
	const NSPoint *souris )
{
	// Est en cours
	if( !this->m_estEnCours )
		return NFALSE;

	// D�finir scrolling
	NDEFINIR_POSITION( this->m_scrolling,
		this->m_scrollingInitial.x - ( this->m_positionClic.x - souris->x ),
		this->m_scrollingInitial.y - ( this->m_positionClic.y - souris->y ) );

	// OK
	return NTRUE;
}

/* Fin scrolling */
void NLib_Module_SDL_Scrolling_NEtatScrolling_FinScrolling( NEtatScrolling *this )
{
	this->m_estEnCours = NFALSE;
}

/* Mettre � z�ro */
void NLib_Module_SDL_Scrolling_NEtatScrolling_MettreZero( NEtatScrolling *this )
{
	// Mettre � z�ro
	this->m_estEnCours = NFALSE;
	memset( &this->m_scrolling,
		0,
		sizeof( NSPoint ) );
}

#endif // NLIB_MODULE_SDL

