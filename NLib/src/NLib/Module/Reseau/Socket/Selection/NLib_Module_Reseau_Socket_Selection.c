#include "../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------------
// namespace NLib::Module::Reseau::Socket::Selection
// -------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Selectionner socket �criture
NBOOL NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( SOCKET socket )
{
	// Descripteur fichier
	fd_set fd;

	// Timeout counter
	struct timeval timeout = { 0, 0 };

	// Zero file descriptor
	FD_ZERO( &fd );

	// Initialiser
	FD_SET( socket,
		&fd );

	// Select socket
	if( select( socket + 1,
		NULL,
		&fd,
		NULL,
		&timeout ) != SOCKET_ERROR )
		return FD_ISSET( socket,
			&fd );

	// La selection a �chou�
	return NFALSE;
}

// Selectionner socket lecture
NBOOL NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( SOCKET socket )
{
	// Descripteur fichier
	fd_set fd;

	// Timeout counter
	struct timeval timeout = { 0, 0 };

	// Zero file descriptor
	FD_ZERO( &fd );

	// Initialiser
	FD_SET( socket,
		&fd );

	// Select socket
	if( select( socket + 1,
		&fd,
		NULL,
		NULL,
		&timeout ) != SOCKET_ERROR )
		return FD_ISSET( socket,
			&fd );

	// La selection a �chou�
	return NFALSE;
}

#endif // NLIB_MODULE_RESEAU

