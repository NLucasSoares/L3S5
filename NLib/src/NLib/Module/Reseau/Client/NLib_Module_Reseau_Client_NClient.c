#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------------------------------
// struct NLib::Module::Reseau::Client::NClient
// --------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/** private **/
/* Callback reception packet */
__CALLBACK NBOOL NLib_Module_Reseau_Client_NClient_CallbackReceptionPacketInterne( const NPacket *packet,
	void *data )
{
	// R�f�rencer
	NREFERENCER( packet );
	NREFERENCER( data );

	// OK
	return NTRUE;
}

/* Callback deconnexion */
__CALLBACK NBOOL NLib_Module_Reseau_Client_NClient_CallbackDeconnexion( void *data )
{
	// R�f�rencer
	NREFERENCER( data );

	// OK
	return NTRUE;
}

/** public **/
/* Construire */
__ALLOC NClient *NLib_Module_Reseau_Client_NClient_Construire( const char *ip,
	NU32 port,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		void* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( void* ),
	void *argumentCallback,
	NU32 tempsAvantTimeout )
{
	// Sortie
	__OUTPUT NClient *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le cache packet
	if( !( out->m_cachePacket = NLib_Module_Reseau_Packet_NCachePacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Configurer la socket
	out->m_addr.sin_family = AF_INET;
	out->m_addr.sin_port = htons( (NU16)port );
	out->m_addr.sin_addr.S_un.S_addr = inet_addr( ip );

	// Enregistrer
	out->m_callbackDeconnexion = ( callbackDeconnexion != NULL ) ? callbackDeconnexion : NLib_Module_Reseau_Client_NClient_CallbackDeconnexion;
	out->m_callbackReceptionPacket = ( callbackReceptionPacket != NULL ) ? callbackReceptionPacket : NLib_Module_Reseau_Client_NClient_CallbackReceptionPacketInterne;
	out->m_argumentCallback = argumentCallback;
	out->m_tempsAvantTimeout = tempsAvantTimeout;

	// Z�ro
	out->m_estConnecte = NFALSE;
	out->m_socket = INVALID_SOCKET;
	out->m_estErreur = NFALSE;

	// Lancer les threads
		// Ca tourne
			out->m_estThreadEnCours = NTRUE;
		// Emission
			if( !( out->m_threadEmission = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)NLib_Module_Reseau_Client_Thread_Emission,
				out,
				0,
				NULL ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// D�truire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Quitter
				return NULL;
			}
		// Reception
			if( !( out->m_threadReception = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)NLib_Module_Reseau_Client_Thread_Reception,
				out,
				0,
				NULL ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// D�truire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Quitter
				return NULL;
			}

	// OK
	return out;
}

/* D�truire */
void NLib_Module_Reseau_Client_NClient_Detruire( NClient **this )
{
	// D�connecter
	NLib_Module_Reseau_Client_NClient_Deconnecter( (*this) );

	// Arr�ter les threads
	(*this)->m_estThreadEnCours = NFALSE;

	// Attendre la fin des threads
	while( WaitForSingleObject( (*this)->m_threadEmission,
			INFINITE )
		|| WaitForSingleObject( (*this)->m_threadReception,
			INFINITE ) )
		NLib_Temps_Attendre( 1 );

	// D�truire le cache packets
	NLib_Module_Reseau_Packet_NCachePacket_Detruire( &(*this)->m_cachePacket );

	// Lib�rer
	NFREE( (*this) );
}

/* Connecter */
NBOOL NLib_Module_Reseau_Client_NClient_Connecter( NClient *this )
{
	// V�rifier �tat
	if( NLib_Module_Reseau_Client_NClient_EstConnecte( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// D�sactiver erreur
	this->m_estErreur = NFALSE;

	// Cr�er la socket
	if( ( this->m_socket = socket( AF_INET,
		SOCK_STREAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Quitter
		return NFALSE;
	}

	// Connecter
	if( connect( this->m_socket,
		(struct sockaddr*)&this->m_addr,
		sizeof( SOCKADDR_IN ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_CONNECTION_FAILED );

		// Fermer la socket
		closesocket( this->m_socket );

		// Quitter
		return NFALSE;
	}

	// Si on a un timeout
	if( this->m_tempsAvantTimeout > 0 )
		// D�finir le timeout
		setsockopt( this->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			(char*)&this->m_tempsAvantTimeout,
			sizeof( NU32 ) );

	// OK
	return this->m_estConnecte = NTRUE;
}

/* D�connecter */
NBOOL NLib_Module_Reseau_Client_NClient_Deconnecter( NClient *this )
{
	// V�rifier connexion
	if( !NLib_Module_Reseau_Client_NClient_EstConnecte( this ) )
		return NFALSE;

	// Callback d�connexion
	this->m_callbackDeconnexion( this->m_argumentCallback );

	// Vider le cache packet
	NLib_Module_Reseau_Packet_NCachePacket_Vider( this->m_cachePacket );

	// On est d�connect�
	this->m_estConnecte = NFALSE;

	// D�truire la socket
	closesocket( this->m_socket );

	// Oublier la socket
	this->m_socket = INVALID_SOCKET;

	// OK
	return NTRUE;
}

/* Activer erreur */
void NLib_Module_Reseau_Client_NClient_ActiverErreur( NClient *this )
{
	this->m_estErreur = NTRUE;
}

/* Ajouter un packet */
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacket( NClient *this,
	__WILLBEOWNED NPacket *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this->m_cachePacket,
		packet );
}

NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacketCopie( NClient *this,
	const NPacket *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( this->m_cachePacket,
		packet );
}

/* Est connect�? */
NBOOL NLib_Module_Reseau_Client_NClient_EstConnecte( const NClient *this )
{
	return this->m_estConnecte;
}

/* Est erreur? */
NBOOL NLib_Module_Reseau_Client_NClient_EstErreur( const NClient *this )
{
	return this->m_estErreur;
}

/* Obtenir cache packet */
NCachePacket *NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( const NClient *this )
{
	return this->m_cachePacket;
}

/* Obtenir socket */
SOCKET NLib_Module_Reseau_Client_NClient_ObtenirSocket( const NClient *this )
{
	return this->m_socket;
}

/* Obtenir est thread en cours */
NBOOL NLib_Module_Reseau_Client_NClient_EstThreadEnCours( const NClient *this )
{
	return this->m_estThreadEnCours;
}

/* Obtenir l'argument pour le callback */
void *NLib_Module_Reseau_Client_NClient_ObtenirArgumentCallback( const NClient *this )
{
	return this->m_argumentCallback;
}

/* Est timeout autoris�? */
NBOOL NLib_Module_Reseau_Client_NClient_EstTimeoutAutorise( const NClient *this )
{
	return this->m_tempsAvantTimeout > 0;
}

/* Obtenir d�lai avant timeout */
NU32 NLib_Module_Reseau_Client_NClient_ObtenirTempsAvantTimeout( const NClient *this )
{
	return this->m_tempsAvantTimeout;
}

#endif // NLIB_MODULE_RESEAU

