#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------------------
// struct NLib::Module::Reseau::Serveur::NServeur
// ----------------------------------------------

#ifdef NLIB_MODULE_RESEAU
// Callbacks par default (private)
__CALLBACK NBOOL NLib_Module_Reseau_Serveur_NServeur_CallbackReceptionPacketDefaut( const NClientServeur *c,
	const NPacket *p )
{
	// R�f�rencer
	NREFERENCER( c );
	NREFERENCER( p );

	// OK
	return NTRUE;
}

__CALLBACK NBOOL NLib_Module_Reseau_Serveur_NServeur_CallbackConnexionDefaut( const NClientServeur *client )
{
	// R�f�rencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

__CALLBACK NBOOL NLib_Module_Reseau_Serveur_NServeur_CallbackDeconnexionDefaut( const NClientServeur *client )
{
	// R�f�rencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

// Construire le serveur
__ALLOC NServeur *NLib_Module_Reseau_Serveur_NServeur_Construire( NU32 port,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NClientServeur*,
		const NPacket* ),
	__CALLBACK NBOOL ( *callbackConnexion )( const NClientServeur* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClientServeur* ),
	void *donneePourClient,
	NU32 tempsAvantTimeout )
{
	// NServeur
	__OUTPUT NServeur *out;

	// Buffer
	char buffer[ 256 ] = { 0, };

	// Cr�er le serveur
	if( !( out = calloc( 1,
		sizeof( NServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Cr�er le mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Param�trer l'adressage
	out->m_adressageServeur.sin_family = AF_INET;
	out->m_adressageServeur.sin_port = htons( (NU16)port );
	out->m_adressageServeur.sin_addr.S_un.S_addr = htonl( INADDR_ANY );

	// Cr�er la socket
	if( ( out->m_socket = socket( AF_INET,
		SOCK_STREAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Fermer mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Bind la socket
	if( bind( out->m_socket,
		(const struct sockaddr*)&out->m_adressageServeur,
		sizeof( SOCKADDR_IN ) ) == SOCKET_ERROR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_BIND );

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Lib�rer la m�moire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_callbackReceptionPacket = ( callbackReceptionPacket != NULL ) ? callbackReceptionPacket : NLib_Module_Reseau_Serveur_NServeur_CallbackReceptionPacketDefaut;
	out->m_callbackConnexion = ( callbackConnexion != NULL ) ? callbackConnexion : NLib_Module_Reseau_Serveur_NServeur_CallbackConnexionDefaut;
	out->m_callbackDeconnexion = ( callbackDeconnexion != NULL ) ? callbackDeconnexion : NLib_Module_Reseau_Serveur_NServeur_CallbackDeconnexionDefaut;
	out->m_donneePourClient = donneePourClient;
	out->m_tempsAvantTimeout = tempsAvantTimeout;

	// Ecouter les clients
	if( listen( out->m_socket,
		5 ) == SOCKET_ERROR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_LISTEN );

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Lib�rer la m�moire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Z�ro
	out->m_nombreClients = 0;
	out->m_nombreMaximumClient = NMAXIMUM_CLIENTS_SIMULTANES_NSERVEUR;
	out->m_tempsDernierUpdateTitre = 0;

	// D�marrer le serveur
	out->m_estEnCours = NTRUE;

	// Cr�er le thread d'acceptation
	if( !( out->m_threadAcceptationClient = CreateThread( NULL,
		0,
		(LPTHREAD_START_ROUTINE)NLib_Module_Reseau_Serveur_Thread_Acceptation_Thread,
		out,
		0,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_THREAD );

		// Arr�ter le serveur
		out->m_estEnCours = NFALSE;

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Lib�rer la m�moire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Connexion autoris�e
	NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( out );

	// Notifier
		// Cr�er le message
			sprintf( buffer,
				"[SERVEUR] Ecoute sur le port %d.",
				port );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// OK
	return out;
}

// D�truire le serveur
void NLib_Module_Reseau_Serveur_NServeur_Detruire( NServeur **serveur )
{
	// Interdire la connexion
	NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( *serveur );

	// Tuer le thread d'acceptation clients
	TerminateThread( (*serveur)->m_threadAcceptationClient,
		EXIT_SUCCESS );

	// Tuer tous les clients
	NLib_Module_Reseau_Serveur_NServeur_TuerTousClients( *serveur );

	// Attendre qu'il n'y ait plus de clients
	while( NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreClients( *serveur ) > 0 )
		// Attendre
		NLib_Temps_Attendre( 1 );

	// Arr�ter le serveur
	(*serveur)->m_estEnCours = NFALSE;

	// Sortie du thread gestion
	while( !(*serveur)->m_flagSortie )
		NLib_Temps_Attendre( 1 );

	// Fermer la socket
	closesocket( (*serveur)->m_socket );

	// Fermer mutex
	NLib_Mutex_NMutex_Detruire( &(*serveur)->m_mutex );

	// Lib�rer le conteneur
	NFREE( *serveur );

	// Changer le titre de la console
#ifdef IS_WINDOWS
	//system( "title Fermeture du serveur." );
#endif // IS_WINDOWS
}

// Est en cours
NBOOL NLib_Module_Reseau_Serveur_NServeur_EstEnCours( const NServeur *this )
{
	return this->m_estEnCours;
}

// Obtenir le nombre de clients
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreClients( const NServeur *this )
{
	// Sortie
	__OUTPUT NU32 nombreClient;

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return 0;
	}

	// Obtenir nombre
	nombreClient = this->m_nombreClients;

	// Release mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Quitter
	return nombreClient;
}

// Obtenir l'index du client avec cet identifiant (priv�e, mutex doit �tre lock)
NU32 NLib_Module_Reseau_Serveur_NServeur_TrouverIndexDepuisIdentifiant( const NServeur *this,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NU32 out = 0;

	// Chercher
	for( ; out < this->m_nombreClients; out++ )
		// Si m�me id
		if( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( this->m_clients[ out ] ) == identifiant )
			return out;

	// Introuvable
	return NERREUR;
}

// Trouver un identifiant unique libre (priv�e, mutex doit �tre lock)
NU32 NLib_Module_Reseau_Serveur_NServeur_TrouverIdentifiantInterne( const NServeur *this )
{
	// Identifiant
	__OUTPUT NU32 identifiant;
	
	// Chercher un identifiant libre
	do
	{
		identifiant = (NU32)NLib_Temps_ObtenirNombreAleatoire( )%LIMITE_IDENTIFIANT_CLIENT_SERVEUR;
	} while( NLib_Module_Reseau_Serveur_NServeur_TrouverIndexDepuisIdentifiant( this,
		identifiant ) != NERREUR );
	
	// OK
	return identifiant;
}

// Ajout de client (private)
NBOOL NLib_Module_Reseau_Serveur_NServeur_AjouterClient( NServeur *this,
	const NClientServeur *nouveauClient )
{
	// Index
	NU32 index;

	// Anciens clients
	NClientServeur **anciensClients;

	// It�rateur
	NU32 i;

	// V�rifier si la connexion est autoris�e
	if( !this->m_estConnexionAutorisee
		|| this->m_nombreClients >= this->m_nombreMaximumClient )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return NFALSE;
	}

	// Trouver un identifiant libre
	NLib_Module_Reseau_Serveur_NClientServeur_DefinirIdentifiantUnique( (NClientServeur*)nouveauClient,
		NLib_Module_Reseau_Serveur_NServeur_TrouverIdentifiantInterne( this ) );	

	// Associer les donn�es devant �tre transmises
	NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne( (NClientServeur*)nouveauClient,
		this->m_donneePourClient );

	// Executer callback
	if( !this->m_callbackConnexion( nouveauClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Il n'y avait aucun client avant
	if( this->m_clients == NULL )
	{
		// Allouer le conteneur
		if( !( this->m_clients = calloc( 1,
			sizeof( NClientServeur* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// D�finir o� enregistrer le client
		index = 0;
	}
	else
	{
		// Enregistrer l'ancien conteneur
		anciensClients = this->m_clients;

		// Allouer le nouveau conteneur
		if( !( this->m_clients = calloc( this->m_nombreClients + 1,
			sizeof( NClientServeur* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Restaurer
			this->m_clients = anciensClients;

			// Release mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// Copier les clients
		for( i = 0; i < this->m_nombreClients; i++ )
			this->m_clients[ i ] = anciensClients[ i ];

		// Lib�rer l'ancien conteneur
		NFREE( anciensClients );

		// D�finir o� enregistrer le client
		index = this->m_nombreClients;
	}

	// Enregistrer le client
	this->m_clients[ index ] = (NClientServeur*)nouveauClient;

	// Incr�menter le nombre de clients
	this->m_nombreClients++;

	// Unlock le mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

// Accepter un client
NBOOL NLib_Module_Reseau_Serveur_NServeur_AccepterClient( NServeur *this )
{
	// Client � ajouter
	NClientServeur *nouveauClient;

	// Buffer
	char buffer[ 256 ] = { 0, };

	// R�cup�rer un nouveau client
	if( !( nouveauClient = NLib_Module_Reseau_Serveur_NClientServeur_Construire( this->m_socket,
		this->m_callbackReceptionPacket,
		&this->m_estConnexionAutorisee,
		this->m_tempsAvantTimeout ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_ACCEPT_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter le client
	if( !NLib_Module_Reseau_Serveur_NServeur_AjouterClient( this,
		nouveauClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Lib�rer le client
		NLib_Module_Reseau_Serveur_NClientServeur_Detruire( &nouveauClient );

		// Quitter
		return NFALSE;
	}

	// Notifier
		// Cr�er le message
			sprintf( buffer,
				"[%s] (%d) se connecte.",
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresseIP( nouveauClient ),
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( nouveauClient ) );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// OK
	return NTRUE;
}

// Obtenir l'index du client dans la liste (private, mutex doit �tre lock)
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirIndexClient( const NServeur *this,
	const NClientServeur *client )
{
	// It�rateur
	__OUTPUT NU32 i = 0;

	// Chercher
	for( ; i < this->m_nombreClients; i++ )
		if( this->m_clients[ i ] == client )
			return i;

	// Introuvable
	return NERREUR;
}

// Supprimer un client (private, mutex doit �tre lock)
NBOOL NLib_Module_Reseau_Serveur_NServeur_SupprimerClient( NServeur *this,
	NU32 index )
{
	// Ancien tableau
	NClientServeur **ancienneAdresse = NULL;

	// It�rateurs
	NU32 i, j;

	// Buffer
	char buffer[ 256 ] = { 0, };

	// V�rifier s'il reste des clients
	if( this->m_nombreClients <= 0 )
		return NFALSE;

	// Callback d�connexion
	this->m_callbackDeconnexion( this->m_clients[ index ] );

	// Notifier
		// Cr�er le message
			sprintf( buffer,
				"[%s] (%d) se deconnecte\n\n",
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresseIP( this->m_clients[ index ] ),
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( this->m_clients[ index ] ) );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// Lib�rer la m�moire
	NLib_Module_Reseau_Serveur_NClientServeur_Detruire( &this->m_clients[ index ] );

	// S'il reste plus d'un client
	if( this->m_nombreClients > 1 )
	{
		// Sauvegarder l'ancienne adresse
		ancienneAdresse = this->m_clients;

		// Allouer une case de moins
		if( !( this->m_clients = calloc( this->m_nombreClients - 1,
			sizeof( NClientServeur* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Restaurer
			this->m_clients = ancienneAdresse;

			// Quitter
			return NFALSE;
		}

		// Copier
		for( i = 0, j = 0; i < this->m_nombreClients; i++ )
			// Pas le client � supprimer?
			if( i != index )
			{
				// Copier adresse
				this->m_clients[ j ] = ancienneAdresse[ i ];
				
				// Incr�menter client
				j++;
			}

		// Lib�rer
		NFREE( ancienneAdresse );
	}
	// Il ne restait qu'un client
	else
		// Lib�rer
		NFREE( this->m_clients );

	// D�cr�menter le nombre de clients
	this->m_nombreClients--;
	
	// OK
	return NTRUE;
}

// Nettoyer les clients morts
NU32 NLib_Module_Reseau_Serveur_NServeur_NettoyerClients( NServeur *this )
{
	// It�rateur
	NU32 i = 0;

	// Nombre de clients nettoy�s
	__OUTPUT NU32 nombreClientNettoye = 0;

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return 0;
	}

	// Chercher les clients morts
	for( ; i < this->m_nombreClients; )
		// Le client est mort?
		if( NLib_Module_Reseau_Serveur_NClientServeur_EstMort( this->m_clients[ i ] ) )
		{
			// Supprimer le client
			NLib_Module_Reseau_Serveur_NServeur_SupprimerClient( this,
				i );

			// Incr�menter le nombre de clients nettoy�s
			nombreClientNettoye++;

			// Recommencer la recherche pour ne rien rater
			i = ( i > 0 ) ?
				i - 1
				: 0;
		}
		else
			i++;

	// Release mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return nombreClientNettoye;
}

void NLib_Module_Reseau_Serveur_NServeur_TuerTousClients( NServeur *this )
{
	// It�rateur
	NU32 i;

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return;
	}

	// Tuer tous les clients
	for( i = 0; i < this->m_nombreClients; i++ )
		NLib_Module_Reseau_Serveur_NClientServeur_Tuer( this->m_clients[ i ] );

	// Release mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

// Thread de nettoyage
void NLib_Module_Reseau_Serveur_NServeur_Update( NServeur *this )
{
	// Nombre de clients nettoy�s
	NU32 clientsNettoyes;

#define TAILLE_BUFFER_TITRE_CONSOLE		2048
#define DELAI_UPDATE_TITRE_CONSOLE		1000 // ms

	// Buffer pour le titre de la console
	char buffer[ TAILLE_BUFFER_TITRE_CONSOLE ];

	// Flag sortie
	this->m_flagSortie = NFALSE;

	// Vider buffer
	memset( buffer,
		0,
		TAILLE_BUFFER_TITRE_CONSOLE );

	// D�finir le titre
	if( NLib_Temps_ObtenirTick( ) - this->m_tempsDernierUpdateTitre >= DELAI_UPDATE_TITRE_CONSOLE )
	{
		// Configurer
		sprintf( buffer,
			"title Serveur (%d client%s) [%s]",
			this->m_nombreClients,
			this->m_nombreClients > 1 ? "s" : "",
			this->m_estConnexionAutorisee ? "OUVERT" : "!!!FERME!!!" );
			
		// D�finir
		system( buffer );

		// Enregistrer temps
		this->m_tempsDernierUpdateTitre = NLib_Temps_ObtenirTick( );
	}

	/* Nettoyage */
	clientsNettoyes = NLib_Module_Reseau_Serveur_NServeur_NettoyerClients( this );

	// Si il y a eu du nettoyage
	if( clientsNettoyes > 0 )
	{
		// Cr�er le message
		sprintf( buffer,
			"[SERVEUR] %d client%s nettoye%s.\n",
				clientsNettoyes,
				( clientsNettoyes > 1 ) ? "s" : "",
				( clientsNettoyes > 1 ) ? "s" : "" );

		// Notifier
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
			buffer,
			0 );
	}

	// Fin update
	this->m_flagSortie = NTRUE;
}

// Changer l'�tat d'autorisation de connexion (priv�e)
void NLib_Module_Reseau_Serveur_NServeur_ChangerEtatConnexion( NServeur *this,
	NBOOL etat )
{
	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
		// Quitter
		return;

	// Changer l'�tat
	this->m_estConnexionAutorisee = etat;

	// Release le mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

// Autoriser la connexion
void NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( NServeur *this )
{
	// Autoriser
	NLib_Module_Reseau_Serveur_NServeur_ChangerEtatConnexion( this,
		NTRUE );
}

// Interdire la connexion
void NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( NServeur *this )
{
	// Interdire
	NLib_Module_Reseau_Serveur_NServeur_ChangerEtatConnexion( this,
		NFALSE );
}

// Obtenir le nombre maximum de clients
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreMaximumClients( const NServeur *this )
{
	// Obtenir
	return this->m_nombreMaximumClient;
}

// D�finir le nombre maximum de clients
void NLib_Module_Reseau_Serveur_NServeur_DefinirNombreMaximumClients( NServeur *this,
	NU32 nb )
{
	// Enregistrer
	this->m_nombreMaximumClient = nb;
}

#endif // NLIB_MODULE_RESEAU

