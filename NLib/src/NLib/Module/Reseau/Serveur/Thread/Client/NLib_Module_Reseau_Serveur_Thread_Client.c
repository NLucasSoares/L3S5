#include "../../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Client
// -------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Thread client serveur */
// Thread emission
void NLib_Module_Reseau_Serveur_Thread_Client_ThreadEmission( NClientServeur *client )
{
	// Packet
	NPacket *packet;

	// Thread
	do
	{
		// D�lais
		NLib_Temps_Attendre( 1 );

		// Si le client est vivant
		if( !NLib_Module_Reseau_Serveur_NClientServeur_EstMort( client ) )
		{
			// Si il y a quelque chose � �crire dans le flux
			if( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) ) > 0 )
				// Si on peut �crire dans le flux
				if( NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ) ) )
				{
					// R�cup�rer le packet
					if( !( packet = NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) ) ) )
						continue;

					// Ecrire dans le flux
					if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacket( packet,
						NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
							"NLib_Module_Reseau_Serveur_Thread_Client_ThreadEmission",
							errno );

						// Tuer le client
						NLib_Module_Reseau_Serveur_NClientServeur_Tuer( client );
					}

					// Lib�rer le packet
					NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

					// Supprimer le packet du cache
					NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) );
				}
		}
	} while( NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( client ) );
}

// Thread reception
void NLib_Module_Reseau_Serveur_Thread_Client_ThreadReception( NClientServeur *client )
{
	// Packet
	NPacket *packet = NULL;

	// Thread
	do
	{
		// D�lais
		NLib_Temps_Attendre( 1 );

		// Si le client est vivant
		if( !NLib_Module_Reseau_Serveur_NClientServeur_EstMort( client ) )
		{
			// Si on peut lire dans le flux
			if( NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ) ) )
			{
				// Lire dans le flux
				if( !( packet = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacket( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ),
					NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutorise( client ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
						"NLib_Module_Reseau_Serveur_Thread_Client_ThreadReception",
						errno );

					// Tuer le client
					NLib_Module_Reseau_Serveur_NClientServeur_Tuer( client );
				}
				// Transmettre au callback
				else
					if( !client->m_callbackReception( client,
						packet ) )
						NLib_Module_Reseau_Serveur_NClientServeur_Tuer( client );

				// Lib�rer le packet
				NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
			}
		}
	} while( NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( client ) );
}

#endif // NLIB_MODULE_RESEAU

