#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------------------------------
// struct NLib::Module::Repertoire::Element::NElementRepertoire
// ------------------------------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE
/* Construire l'�l�ment */
__ALLOC NElementRepertoire *NLib_Module_Repertoire_Element_NElementRepertoire_Construire( const char *nom,
	NU32 tailleFichier,
	NAttributRepertoire	attributs,
	NU64 dateCreation,
	NU64 dateModification,
	NU64 dateAcces )
{
	// Sortie
	__OUTPUT NElementRepertoire *out;

	// Taille nom
	NU32 tailleNom;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NElementRepertoire ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
		
		// Quitter
		return NULL;
	}

	// Calculer taille nom
	tailleNom = strlen( nom );

	// Allouer la m�moire
	if( !( out->m_nom = calloc( tailleNom + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// R�cup�rer chemin absolu
	if( !( out->m_cheminAbsolu = NLib_Module_Repertoire_ObtenirCheminAbsolu( nom ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_CURRENT_DIRECTORY_FAILED );

		// Lib�rer
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Stocker informations
		// Nom
			memcpy( out->m_nom,
				nom,
				tailleNom );
		// Taille
			out->m_taille = tailleFichier;
		// Date
			// Cr�ation
				out->m_dateCreation = dateCreation;
			// Modification
				out->m_dateModification = dateModification;
			// Acc�s
				out->m_dateDernierAcces = dateAcces;
		// Attributs
			out->m_attribut = attributs;

	// OK
	return out;
}


/* D�truire �l�ment */
void NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( NElementRepertoire **this )
{
	// D�truire
	NFREE( (*this)->m_nom );
	NFREE( (*this)->m_cheminAbsolu );

	// Lib�rer
	NFREE( *this );
}

/* Obtenir nom */
const char *NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom( const NElementRepertoire *this )
{
	return this->m_nom;
}

/* Obtenir chemin absolu vers le fichier */
const char *NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirCheminAbsolu( const NElementRepertoire *this )
{
	return this->m_cheminAbsolu;
}

/* Obtenir taille */
NU32 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirTaille( const NElementRepertoire *this )
{
	return this->m_taille;
}

/* Obtenir date cr�ation */
NU64 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirDateCreation( const NElementRepertoire *this )
{
	return this->m_dateCreation;
}

/* Obtenir date modification */
NU64 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirDateModification( const NElementRepertoire *this )
{
	return this->m_dateModification;
}

/* Obtenir date acc�s */
NU64 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirDateDernierAcces( const NElementRepertoire *this )
{
	return this->m_dateDernierAcces;
}

/* Obtenir attributs */
NAttributRepertoire NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirAttributs( const NElementRepertoire *this )
{
	return this->m_attribut;
}

/* Est un r�pertoire? */
NBOOL NLib_Module_Repertoire_Element_NElementRepertoire_EstRepertoire( const NElementRepertoire *this )
{
	return this->m_attribut & _A_SUBDIR;
}

/* Est un fichier? */
NBOOL NLib_Module_Repertoire_Element_NElementRepertoire_EstFichier( const NElementRepertoire *this )
{
	return !NLib_Module_Repertoire_Element_NElementRepertoire_EstRepertoire( this );
}

#endif // NLIB_MODULE_REPERTOIRE

