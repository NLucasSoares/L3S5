#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -----------------------
// namespace NLib::Memoire
// -----------------------

/* R�allocation */
NBOOL NLib_Memoire_ReallouerMemoire( char **src,
	NU32 tailleSrc,
	NS32 delta )
{
	// Espace temporaire
	void *tmp = NULL;

	// V�rifier param�tres
	if( (NS32)tailleSrc + delta <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Si d�j� allou�
	if( tailleSrc )
	{
		// Sauvegarder
		tmp = *src;

		// Allouer la m�moire
		if( !( *src = calloc( (NU32)( tailleSrc + delta ),
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Restaurer
			*src = tmp;

			// Quitter
			return NFALSE;
		}

		// Copier ancienne donn�es
		memcpy( *src,
			tmp,
			tailleSrc >= tailleSrc + delta ?
				tailleSrc + delta
				: tailleSrc );

		// Lib�rer
		NFREE( tmp );
	}
	// Allouer pour la premi�re fois
	else
		// Le delta est bien strictement positif
		if( delta > 0 )
		{
			// Allouer
			if( !( *src = calloc( delta,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}
		}
		// Delta <= 0
		else
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quitter
			return NFALSE;
		}

	// OK
	return NTRUE;
}

/* Ajouter des donn�es */
NBOOL NLib_Memoire_AjouterData( char **mem,
	NU32 tailleMem,
	const char *memAjout,
	NU32 tailleMemAjout )
{
	// Augmenter la taille disponible
	if( !NLib_Memoire_ReallouerMemoire( mem,
		tailleMem,
		tailleMemAjout ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quitter
		return NFALSE;
	}

	// Copier les nouvelles donn�es
	memcpy( *mem + tailleMem,
		memAjout,
		tailleMemAjout );

	// OK
	return NTRUE;
}

/* Calculer CRC32 */
NU32 NLib_Memoire_CalculerCRC( const char *data,
	NU32 taille )
{
	// Sortie
	__OUTPUT NU32 out = 0;

	// It�rateur
	NU32 i = 0;

	// Calculer
	for( ; i < taille; i++ )
		out += *( data + i );

	// OK
	return out;
}

/* Swap */
NBOOL NLib_Memoire_Swap( void *d1,
	void *d2,
	NU32 taille )
{
	// Temp
	char *temp;

	// Allouer la m�moire
	if( !( temp = calloc( taille,
		sizeof( char ) ) ) )
		return NFALSE;

	// Swap
		// temp<-d1;
			memcpy( temp,
				d1,
				taille );
		// d1<-d2
			memcpy( d1,
				d2,
				taille );
		// d2<-temp
			memcpy( d2,
				temp,
				taille );

	// Lib�rer
	NFREE( temp );

	// OK
	return NTRUE;
}

void NLib_Memoire_SwapEntier( NU32 *d1,
	NU32 *d2 )
{
	// Temp
	NU32 d3;

	// Swap
	d3 = *d1;
	*d2 = *d1;
	*d1 = d3;
}

