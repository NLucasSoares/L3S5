#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------
// namespace NLib::Math::Geometrie
// -------------------------------

/* Intersection entre deux zones rectangulaires */
NSRect NLib_Math_Geometrie_CalculerIntersection( NSPoint p1,
	NUPoint t1,
	NSPoint p2,
	NUPoint t2 )
{
	// Sortie
	NSRect sortie = { { 0, 0 },
		{ 0, 0 } };

	// Si il n'y a pas colision
	if( !NLib_Math_Geometrie_EstColision( p1,
		t1,
		1,
		p2,
		t2,
		1 ) )
		// Intersection nulle
		return sortie;
	else
	{
		// Composer
		NDEFINIR_POSITION( sortie.m_position,
			NLib_Math_Minimum( p1.x,
				p2.x ),
			NLib_Math_Minimum( p1.y,
				p2.y ) );

		NDEFINIR_POSITION( sortie.m_taille,
			NLib_Math_Maximum( (NU32)0,
				(NU32)( NLib_Math_Minimum( p1.x + (NS32)t1.x, p2.x + (NS32)t2.x ) - NLib_Math_Maximum( p1.x, p2.x ) ) ),
			NLib_Math_Maximum( (NU32)0,
				(NU32)( NLib_Math_Minimum( p1.y + (NS32)t1.y, p2.y + (NS32)t2.y ) - NLib_Math_Maximum( p1.y, p2.y ) ) ) );


		// OK
		return sortie;
	}
}

/* Est intersection nulle? */
NBOOL NLib_Math_Geometrie_EstIntersectionNulle( NSRect i )
{
	return ( i.m_position.x == 0
		&& i.m_position.y == 0
		&& i.m_taille.x == 0
		&& i.m_taille.y == 0 );
}

/* Collision entre deux zones rectangulaires? */
NBOOL NLib_Math_Geometrie_EstColision( NSPoint p1,
	NUPoint t1,
	NU32 epaisseurC1,
	NSPoint p2,
	NUPoint t2,
	NU32 epaisseurC2 )
{
	return ( p1.x < p2.x + (NS32)( t2.x + epaisseurC2 )
		&& p1.x + (NS32)( t1.x + epaisseurC1 ) > p2.x
		&& p1.y < p2.y + (NS32)( t2.y + epaisseurC2 )
		&& p1.y + (NS32)( t1.y + epaisseurC1 ) > p2.y );
}

/* Colision entre un rectangle et un point? */
NBOOL NLib_Math_Geometrie_EstColision2( NSPoint p1,
	NUPoint t1,
	NSPoint p2 )
{
	return ( p2.x > p1.x
		&& p2.y > p1.y
		&& p2.x < p1.x + (NS32)t1.x
		&& p2.y < p1.y + (NS32)t1.y );
}

