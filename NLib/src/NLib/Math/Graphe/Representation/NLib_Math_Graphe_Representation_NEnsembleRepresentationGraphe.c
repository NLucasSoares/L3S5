#include "../../../../../include/NLib/NLib.h"

// -----------------------------------------------------------------------
// union NLib::Math::Graphe::Representation::NEnsembleRepresentationGraphe
// -----------------------------------------------------------------------

/* Destruction d'un graphe */
void NLib_Math_Graphe_Representation_NEnsembleRepresentationGraphe_Detruire( NEnsembleRepresentationGraphe *ensemble,
	NTypeRepresentationGraphe typeRepresentation )
{
	switch( typeRepresentation )
	{
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_MATRICE_ADJACENCE:
			NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_Detruire( &ensemble->m_matriceAdjacence );
			break;

		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_FILE_SUCCESSEUR:
			NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_Detruire( &ensemble->m_fileSuccesseur );
			break;

		default:

			break;
	}
}

